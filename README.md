# JS Sketch

## Overview
This project's purpose is to act as a sandbox for implementing and testing various programming concepts.
The idea is that these projects can be used as reference when creating more complex systems.

## Sketches

### Collision
An implementation of SAT (Separating Axis Theorem) also includes collision points with collision surface normals.

![](preview_collision.gif)

### Marching Squares
An implementation of marching squares that makes use of a texture atlas to render a 2d landscape.

![](preview_marchingsquares.gif)

### Spatial Indexing
A sketch that implements an N-Tree (a quad tree in this example) used to accelerate ray marching.

![](preview_spatialindexing.gif)

### Nav Mesh
I have seen a lot of triangular 'Navmesh' implementations and thought it would be fun to write a rectangle based one from scratch.

The heuristic calculations are quite interesting as you cannot simply use a rectangle's center point. The aproach I came up with and have currently implemented is to take the previous rectangle's "point" and clamp it within the neighbor rectangle which is seeded by the original position.

```
s = start      =>    s = start, p = s clamped to p's rect
______         =>    ______      
|  s |         =>    |  s |      
|    |____     =>    |    |____  
|    |    |    =>    |    |p   | <- p ends up in the top left corner as it is the closest point within its rect
|____|____|    =>    |____|____|    this effect trickles through as neighbors are added to the open set
```

![](preview_nav_mesh.gif)

### Factory
A sketch inspired by [Mindustry](https://mindustrygame.github.io/) and other factory simmulator type games.

![](preview_factory.gif)

### Physics
A work in progress sketch inspired by wanting to understand at a fundamental level how physics engines work.

![](preview_physics.gif)

### N-Body Gravity
A spur of the moment project, no real reason I picked this specifically, just seemed fun at the time.

![](preview_nbodygravity.gif)

### Image Processing
A sketch implementing the [Hough Transform](https://en.wikipedia.org/wiki/Hough_transform) from scratch in javascript.

![](preview_imageprocessing.gif)


## Getting Started
The following instructions will get you up and running to try these projects out on your own system.

### Prerequisites

To use this project you'll need:

* [nodejs](https://nodejs.org/en/) - (required) The environment the project is built upon.
* [git](https://git-scm.com/) - (optional) If you wish to clone this project, you can also just download the repo instead.
* [vscode](https://code.visualstudio.com/) - (optional) Useful text editor if you wish to delve into the code and don't have an editor of choice already.

If using git, you can use the command:
```
git clone https://gitlab.com/ilmaestroh4x/js-sketch.git
```

### Installing NPM dependencies

Any required node modules such as [electron](https://www.electronjs.org/) can be installed using the `npm i` command which will grab all required packages.

### Running project

* If you're NOT using [vscode](https://code.visualstudio.com/), you can use the `npm start` command from a terminal that is open in the project's root folder.
* If you ARE using [vscode](https://code.visualstudio.com/), simply open the the root folder in vscode, and press 'f5' or click on the run arrow on the left side and click the green launch arrow at the top of the run tab.

## License

See 'LICENSE' in project root.