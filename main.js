const electron = require('electron')
const path = require('path')

const { app, BrowserWindow } = electron
const appDir = path.dirname(require.main.filename)

let win

app.once('ready', createWindow)
app.on('activate', () => { if (win === null) createWindow() })
app.once('window-all-closed', () => { if (process.platform !== 'darwin') app.quit() })

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 1600,
        height: 900,
        show: false,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.once('ready-to-show', win.show)
	
    // and load the index.html of the app.
    win.loadFile(path.resolve(appDir, 'Projects', 'webgl', 'index.html'))

    // Open the DevTools.
    win.webContents.openDevTools()

    win.on('closed', () => { win = null })
}