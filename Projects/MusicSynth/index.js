const OSCILLATOR_TYPES = [
    'sine',
    'square',
    'triangle',
    'sawtooth'
]

const NOTES = {
    'C' : 16.35,
    'C#': 17.32,
    'D' : 18.35,
    'Eb': 19.45,
    'E' : 20.60,
    'F' : 21.83,
    'F#': 23.12,
    'G' : 24.50,
    'G#': 25.96,
    'A' : 27.50,
    'Bb': 29.14,
    'B' : 30.87,
}

function create_notes(max_octave = 9) {
    let result = {}

    const OCTAVES = [...Array(max_octave)].map((_, octave) => Math.pow(2, octave))

    Object.entries(NOTES).forEach(([note, base_frequency]) => {
        result[note] = OCTAVES.map(octave => base_frequency * octave)
    })

    return result
}

function create_instruments(notes, audioContext = new AudioContext()) {
    let result = {}

    OSCILLATOR_TYPES.forEach(oscillator_type => {
        result[oscillator_type] = {}

        Object.entries(notes).forEach(([note, frequencies]) => {
            result[oscillator_type][note] = frequencies.map(frequency => {
                let oscillator_node = audioContext.createOscillator()
                oscillator_node.type = oscillator_type
                
                let gain_node = audioContext.createGain()

                oscillator_node.connect(gain_node)

                gain_node.connect(audioContext.destination)

                oscillator_node.frequency.value = frequency

                gain_node.gain.setValueAtTime(0, audioContext.currentTime)

                oscillator_node.start()

                return function() {
                    //gain_node.gain.setValueAtTime(gain_node.gain.value, audioContext.currentTime)
                    gain_node.gain.exponentialRampToValueAtTime(1, audioContext.currentTime + 0.01)
                    gain_node.gain.exponentialRampToValueAtTime(0.0001, audioContext.currentTime + 1)
                }
            })
        })
    })



    return result
}

let audio_ctx = new AudioContext()
let instruments = create_instruments(create_notes(), audio_ctx)


let grid_w = 32
let grid_h = 8
let grid = Array.create_matrix(grid_w, grid_h)
grid.deep_map(() => 0)

let cell_w = canvas.width  / grid_w
let cell_h = canvas.height / grid_h
let cell_size = [cell_w, cell_h]

window.addEventListener('resize', e => {
    cell_w = canvas.width  / grid_w
    cell_h = canvas.height / grid_h
    cell_size = [cell_w, cell_h]
})

let note_colors = [...Array(Object.keys(NOTES).length + 1)].map(() => '#' + Math.floor(Math.random()*16777215).toString(16))

let mouse_pos  = [0, 0]
let mouse_cell = [0, 0]

canvas.addEventListener('mousemove', e => {
    mouse_pos = [e.offsetX, e.offsetY]
    mouse_cell = mouse_pos.divide(cell_size).map(Math.floor)
})

canvas.addEventListener('mousedown', e => {
    mouse_pos = [e.offsetX, e.offsetY]
    mouse_cell = mouse_pos.divide(cell_size).map(Math.floor)

    let previous = grid.get_at(mouse_cell)
    previous = (previous + 1) % (Object.keys(NOTES).length + 1)
    grid.set_at(mouse_cell, previous)
})

let last_column = -1
new Simulation((gt, dt) => {
    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())


    let x = (gt * 0.5) % canvas.width

    let column = Math.floor(x / cell_w)
    
    let play_note = false
    if (last_column != column) {
        last_column = column
        play_note = true
    }
    
    ctx.fillStyle = 'rgba(255,0,0,0.3)'
    ctx.fillRect(column * cell_w, 0, cell_w, canvas.height)

    ctx.fillStyle = 'white'
    grid.deep_forEach((cell, pos) => {
        if (cell != 0) {
            let note = Object.keys(NOTES)[cell - 1]
            if (pos[0] == column && play_note) {
                instruments['sine'][note][pos[1]]()
            }
            ctx.fillStyle = note_colors[cell - 1]
            ctx.fillRect(...pos.multiply(cell_size), ...cell_size)
        }
    })

    ctx.strokeStyle = 'red'
    ctx.lineWidth = 3
    ctx.beginPath()
    ctx.moveTo(x, 0)
    ctx.lineTo(x, canvas.height)
    ctx.stroke()

    ctx.strokeStyle = 'white'
    ctx.strokeRect(...mouse_cell.multiply(cell_size), ...cell_size)
})