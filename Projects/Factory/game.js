const atlas = document.getElementById('tex2d')

/*
    todo, change to a node based system where each tile 'connects' to targets, and from sources
    this will let you easily trace paths, and whatever object is infront of the current one, etc
*/

function create_enum(...keys) {
    return keys.reduce((obj, key, value) => {
        obj[key] = value
        return obj
    }, {})
}

const TILE_TYPE = create_enum(
    'EMPTY',
    'CONVEYOR_UP',
    'CONVEYOR_RIGHT',
    'CONVEYOR_DOWN',
    'CONVEYOR_LEFT',

    'size'// not actual type, just to know how many types there are.
)

class Item {
    constructor(position, type) {
        this.position = position
        this.type = type
    }

    get_tile_position() {
        return this.position.map(Math.round)
    }

    move_towards(target, step_size) {
        const direction = target.subtract(this.position).normalize()
        this.position = this.position.add(direction.multiply(step_size))
    }

    render(ctx) {
        ctx.drawImage(atlas, 0, 32, 32, 32, 0, 0, 1, 1)
    }
}

class Tile {
    constructor(position, type) {
        this.position = position
        this.type = type
        this.items = []
    }

    item_add(id) {
        const index = this.items.indexOf(id)
        if (index < 0) {
            this.items.push(id)
            return true
        }
        return false
    }

    item_remove(id) {
        const index = this.items.indexOf(id)
        if (index >= 0) {
            this.items.splice(index, 1)
            return true
        }
        return false
    }

    update(item, delta_time) {
        const speed = 0.0025
        const direction = [ 0.0, 0.0 ]

        let is_move_type = false
        switch (this.type) {
            case TILE_TYPE.CONVEYOR_UP:
                direction[1] = -0.5
                is_move_type = true
                break
            case TILE_TYPE.CONVEYOR_RIGHT:
                direction[0] =  0.5
                is_move_type = true
                break
            case TILE_TYPE.CONVEYOR_DOWN:
                direction[1] =  0.5
                is_move_type = true
                break
            case TILE_TYPE.CONVEYOR_LEFT:
                direction[0] = -0.5
                is_move_type = true
                break
        }

        if (is_move_type) {
            const item_target = this.position.add(direction)
            item.move_towards(item_target, delta_time * speed)
        }
    }

    render(ctx) {
        let x = 0
        let y = 0

        switch (this.type) {
            case TILE_TYPE.CONVEYOR_UP:
                x = 32
                break
            case TILE_TYPE.CONVEYOR_RIGHT:
                x = 64
                break
            case TILE_TYPE.CONVEYOR_DOWN:
                x = 64
                y = 32
                break
            case TILE_TYPE.CONVEYOR_LEFT:
                x = 32
                y = 32
                break
        }

        ctx.drawImage(atlas, x, y, 32, 32, 0, 0, 1, 1)
    }
}

class World {
    constructor(tile_cols, tile_rows, tile_size) {
        this.tiles = Array.create_matrix(tile_cols, tile_rows).deep_map((_, position) => {
            return new Tile(position, TILE_TYPE.EMPTY)
        })
        this.tile_size = tile_size
        this.tile_cols = tile_cols
        this.tile_rows = tile_rows

        this.items = []
    }

    load_map(data) {
        this.tiles.deep_forEach((tile, position) => {
            this.update_tile_type(tile, data.get_at(position))
        })
    }

    add_item_to_tile(tile, item_type) {
        if (tile.item_add(this.items.length)) {
            this.items.push(new Item(tile.position, item_type))
        }
    }

    get_tile_at_mouse_position(mouse_position) {
        const tile_position = mouse_position.divide(this.tile_size).map(Math.floor)
        if (tile_position[0] >= 0 && tile_position[0] < this.tile_cols &&
            tile_position[1] >= 0 && tile_position[1] < this.tile_rows) {
            return this.tiles.get_at(tile_position)
        } else {
            return null
        }
    }

    update_tile_type(tile, new_tile_type) {
        tile.type = new_tile_type

        //todo deal w/ tile items etc as well to delete on type change
        //if (tile.type != new_tile_type) {
        //    this.tiles.set_at(tile.position, new Tile(tile.position, new_tile_type))
        //}
    }

    update(delta_time) {
        this.items.forEach((item, index) => {
            
            const curr_tile_position = item.get_tile_position()
            const curr_tile = this.tiles.get_at(curr_tile_position)

            curr_tile.update(item, delta_time)

            const next_tile_position = item.get_tile_position()
            const next_tile = this.tiles.get_at(next_tile_position)

            // item has changed tiles.
            if (!curr_tile_position.equals(next_tile_position)) {
                curr_tile.item_remove(index)
                next_tile.item_add(index)
            }
        })
    }

    render(ctx) {
        ctx.save()

        // scale whole scene context up
        ctx.scale(this.tile_size, this.tile_size)

        ctx.fillStyle = '#113'
        ctx.fillRect(0, 0, this.tile_cols, this.tile_rows)

        // draw tiles
        this.tiles.deep_forEach(tile => {
            ctx.save()
           
            ctx.translate(...tile.position)

            tile.render(ctx)
          
            ctx.restore()
        })

        // draw items
        this.items.forEach(item => {
            ctx.save()

            ctx.translate(...item.position)

            item.render(ctx)

            ctx.restore()
        })

        ctx.restore()
        
        ctx.fillStyle = 'white'
        ctx.font = "12px Arial";
        ctx.fillText(`Item count: ${this.items.length}`, 2, 14)
    }

    render_cursor(ctx, mouse_position) {
        const hover_tile = this.get_tile_at_mouse_position(mouse_position)
        if (hover_tile) {
            ctx.save()

            ctx.scale(this.tile_size, this.tile_size)
            
            ctx.translate(...hover_tile.position)

            ctx.fillStyle = '#FFF6'
            ctx.fillRect(0, 0, 1, 1)
        
            ctx.restore()
        }
    }
}