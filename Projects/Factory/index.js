const map_data = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,2,1,1,1,1,1,0,0,0,0,0,0,0,0,0],[0,2,0,0,3,3,4,0,2,1,1,1,1,1,0,0],[0,2,0,0,4,0,0,0,2,0,0,0,0,4,0,0],[0,2,0,0,4,1,0,2,1,0,0,0,0,4,0,0],[0,3,3,2,0,4,0,3,3,2,0,0,0,4,0,0],[0,2,1,1,0,4,0,0,0,2,0,3,3,4,0,0],[0,2,0,0,0,4,0,0,0,2,0,4,1,0,0,0],[0,3,3,2,0,4,0,0,0,2,3,2,4,0,0,0],[0,2,1,1,0,4,0,0,0,2,4,2,4,0,0,0],[0,2,0,3,2,4,0,0,0,2,4,2,4,1,0,0],[0,3,3,4,2,4,1,1,1,1,4,3,2,4,0,0],[0,2,1,1,3,3,3,3,3,3,4,2,1,4,0,0],[0,2,0,4,1,1,1,1,1,1,1,1,0,4,0,0],[0,3,3,3,3,3,3,3,3,3,3,3,3,4,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]

const world = new World(16, 16, 32)
world.load_map(map_data)

let mouse_position = [0, 0]
 
canvas.addEventListener('mousemove', e => {
    mouse_position = [e.clientX, e.clientY]
})

canvas.addEventListener('mouseup', e => {
    const tile = world.get_tile_at_mouse_position([e.clientX, e.clientY])
    if (tile) {
        if (e.button == 0) {
            // just cycle through the types for now.
            const new_tile_type = (tile.type + 1) % TILE_TYPE.size
            world.update_tile_type(tile, new_tile_type)
        } else {
            world.add_item_to_tile(tile, 0)
        }
    }
})

ctx.imageSmoothingEnabled = false
new Simulation((gt, dt) => {
    dt = Math.min(dt, 33)

    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    world.update(dt)

    world.render(ctx)
    world.render_cursor(ctx, mouse_position)
})