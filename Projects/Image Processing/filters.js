const filter = {
    outline: {
        width: 3,
        height: 3,
        normalize: false,
        Kernel: [
            [-1, -1, -1],
            [-1, 8, -1],
            [-1, -1, -1],
        ]
    },
    sharpen: {
        width: 3,
        height: 3,
        normalize: false,
        Kernel: [
            [0, -1, 0],
            [-1, 5, -1],
            [0, -1, 0],
        ]
    },
    blur: {
        width: 3,
        height: 3,
        normalize: true,
        Kernel: [
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1],
        ]
    },
    GX: {
        width: 2,
        height: 2,
        normalize: true,
        Kernel: [
            [1, 0],
            [0, -1],
        ]
    },
    GY: {
        width: 2,
        height: 2,
        normalize: true,
        Kernel: [
            [0, 1],
            [-1, 0],
        ]
    },
    gauss5x5: {
        width: 5,
        height: 5,
        normalize: true,
        Kernel: [
            [2, 4, 5, 4, 2],
            [4, 9, 12, 9, 4],
            [5, 12, 15, 12, 5],
            [4, 9, 12, 9, 4],
            [2, 4, 5, 4, 2],
        ]
    },
    sx: {
        width: 3,
        height: 3,
        normalize: false,
        Kernel: [
            [-1, 0, 1],
            [-2, 0, 2],
            [-1, 0, 1],
        ],
        map: {
            from: [-1, 1],
            to: [0, 1]
        }
    },
    sy: {
        width: 3,
        height: 3,
        normalize: false,
        Kernel: [
            [ 1,  2,  1],
            [ 0,  0,  0],
            [-1, -2, -1],
        ],
        map: {
            from: [-1, 1],
            to: [0, 1]
        }
    }
}