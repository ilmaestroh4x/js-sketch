function toDegrees(angle) {
    return angle * (180 / Math.PI)
}

function toRadians(angle) {
    return angle * (Math.PI / 180)
}

function toCompass(rads) {
    const SLICE_NAMES = [
        'East',
        'NorthEast',
        'North',
        'NorthWest',
        'West',
        'SouthWest',
        'South',
        'SouthEast'
    ]
    const SLICE_COUNT = SLICE_NAMES.length
    const SLICE_SIZE = 2 * Math.PI / SLICE_COUNT
    let result = Math.round(rads / SLICE_SIZE)
    result = (result + SLICE_COUNT) % SLICE_COUNT
    return {
        sliceIndex: result,
        sliceName: SLICE_NAMES[result],
        nearestAngle: result * SLICE_SIZE
    }
}

function mapRange(value, sLow, sHigh, eLow, eHigh) {
    return (value - sLow) * ((eHigh - eLow) / (sHigh - sLow)) + eLow
}

function rotate(x, y, angle) {
    return [
        (x * Math.cos(angle)) - (y * Math.sin(angle)),
        (y * Math.cos(angle)) + (x * Math.sin(angle))
    ]
}

function vecLength(vector) {
    let m = 0
    for (let i = 0; i < vector.length; i++) {
        m += Math.pow(vector[i], 2)
    }
    return Math.sqrt(m)
}

function vecNormalize(vector) {
    let result = new Array(vector.length)
    const len = vecLength(vector)
    for (let i = 0; i < vector.length; i++) {
        result[i] = vector[i] / len
    }
    return result
}

function vecAdd(v1, v2) {
    if (v1.length !== v2.length) return 0
    let result = new Array(v1.length)
    for (let i = 0; i < v1.length; i++) {
        result[i] = v1[i] + v2[i]
    }
    return result
}

function vecSub(v1, v2) {
    if (v1.length !== v2.length) return 0
    let result = new Array(v1.length)
    for (let i = 0; i < v1.length; i++) {
        result[i] = v1[i] - v2[i]
    }
    return result
}

function vecDot(v1, v2) {
    if (v1.length !== v2.length) return 0
    let result = 0
    for (let i = 0; i < v1.length; i++) {
        result += v1[i] * v2[i]
    }
    return result
}

function distance(p1, p2) {
    return vecLength([
        p2.x - p1.x,
        p2.y - p1.y
    ])
}

function createMatrix(rows, cols, defaultValue = 0) {
    return [...Array(rows)].map(x=>Array(cols).fill(defaultValue))
}

function clamp(value, min, max) {
    return Math.max(min, Math.min(max, value))
}

async function loadImage(url) {
    return new Promise((resolve, reject) => {
        let img = new Image()
        img.onload = () => resolve(img)
        img.onerror = reject
        img.src = url
    })
}

ImageData.prototype.clone = function() {
    return new ImageData(
        new Uint8ClampedArray(this.data),
        this.width,
        this.height
    )
}

ImageData.prototype.getPixel = function({x, y}) {
    const i = (y * this.width + x) * 4
    return {
        r: this.data[i + 0] / 255.0,
        g: this.data[i + 1] / 255.0,
        b: this.data[i + 2] / 255.0,
        a: this.data[i + 3] / 255.0,
    }
}

ImageData.prototype.setPixel = function({x, y}, {r, g, b, a}) {
    const i = (y * this.width + x) * 4
    this.data[i + 0] = Math.floor(r * 255)
    this.data[i + 1] = Math.floor(g * 255)
    this.data[i + 2] = Math.floor(b * 255)
    this.data[i + 3] = Math.floor(a * 255)
}

ImageData.prototype.filter = function(filters) {
    let imgBuffer = [
        this.clone(),
        this.clone(),
    ]

    let imgBufferIndex = 0
    let readBuffer     = imgBuffer[imgBufferIndex]
    let writeBuffer    = imgBuffer[imgBufferIndex]

    for (let pass = 0; pass < filters.length; pass++) {
        let filter = filters[pass]

        if (typeof filter === 'function') {
            for (let x = 0; x < readBuffer.width; x++) {
                for (let y = 0; y < readBuffer.height; y++) {
                    let position = {x, y}
                    let color = readBuffer.getPixel(position)
                    color = filter(color, position)
                    writeBuffer.setPixel(position, color)
                }
            }
        } else {
            let halfFilterWidth  = Math.floor(filter.width  / 2)
            let halfFilterHeight = Math.floor(filter.height / 2)

            if (halfFilterWidth  == 0) halfFilterWidth  = 1
            if (halfFilterHeight == 0) halfFilterHeight = 1

            imgBufferIndex = pass

            readBuffer  = imgBuffer[imgBufferIndex & 1]
            writeBuffer = imgBuffer[(imgBufferIndex + 1) & 1]

            let mapper = null
            if (filter.map) {
                mapper = v => {
                    return mapRange(v,
                        ...filter.map.from,
                        ...filter.map.to)
                }
            }

            for (let x = 0; x < readBuffer.width; x++) {
                for (let y = 0; y < readBuffer.height; y++) {
                    let color = { r: 0, g: 0, b: 0, a: 1 }

                    if (x - halfFilterWidth  >= 0 && x + halfFilterWidth  < readBuffer.width &&
                        y - halfFilterHeight >= 0 && y + halfFilterHeight < readBuffer.height) {

                        let kernelTotal = 0
                        
                        for (let fx = 0; fx < filter.width; fx++) {
                            for (let fy = 0; fy < filter.height; fy++) {
                                let k = filter.Kernel[fx][fy]

                                kernelTotal += k

                                let pos = {
                                    x: x + (fx - halfFilterWidth),
                                    y: y + (fy - halfFilterHeight)
                                }

                                let { r, g, b } = readBuffer.getPixel(pos)
                                
                                color.r += r * k
                                color.g += g * k
                                color.b += b * k
                            }
                        }

                        if (filter.normalize) {
                            color.r /= kernelTotal
                            color.g /= kernelTotal
                            color.b /= kernelTotal
                        }
                    } else {
                        let { r, g, b } = readBuffer.getPixel({x, y})

                        color.r = r
                        color.g = g
                        color.b = b
                    }

                    if (mapper) {
                        color.r = mapper(color.r)
                        color.g = mapper(color.g)
                        color.b = mapper(color.b)
                    }

                    writeBuffer.setPixel({x, y}, color)
                }
            }
        }
    }

    return writeBuffer
}
