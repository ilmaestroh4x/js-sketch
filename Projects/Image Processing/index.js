function loadImageData(img) {
    const canvas = document.createElement('canvas')
    canvas.width = img.width
    canvas.height = img.height
    
    console.log('Applying pre-processing filters...')
    const ctx = canvas.getContext('2d')
    ctx.drawImage(img, 0, 0)

    let imgData = ctx.getImageData(0, 0, canvas.width, canvas.height)

    //https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123

    imgData = imgData.filter([
        function(color) {
            let lum =
                0.2126 * color.r +
                0.7152 * color.g +
                0.0722 * color.b
            color.r = lum
            color.g = lum
            color.b = lum
            return color
        },
        filter.gauss5x5,
        //filter.gauss5x5,
        //filter.gauss5x5,
    ])

    let imgData_fx = imgData.filter([ filter.sx ])
    let imgData_fy = imgData.filter([ filter.sy ])

    let edgeDir = createMatrix(canvas.width, canvas.height)
    let gradient = createMatrix(canvas.width, canvas.height)

    imgData.filter([
        function(color, pos) {
            let ix = imgData_fx.getPixel(pos).r * 2 - 1
            let iy = imgData_fy.getPixel(pos).r * 2 - 1

            edgeDir[pos.x][pos.y] = toCompass(Math.atan2(iy, ix)).sliceIndex
            gradient[pos.x][pos.y] = Math.hypot(ix, iy)

            return color
        }
    ])

    let suppression = createMatrix(canvas.width, canvas.height)

    let highThreshold = 0
    let lowThreshold = 1
    let gradiantBuckets = Array(255).fill(0)
    
    function colorToGBIndex(color) {
        const MAX = gradiantBuckets.length - 1
        return clamp(Math.round(color.r * MAX), 0, MAX)
    }

    for (let x = 1; x < canvas.width - 1; x++) {
        for (let y = 1; y < canvas.height - 1; y++) {
            let q = 1
            let r = 1

            switch(edgeDir[x][y] % 4) {
                case 0:
                    q = gradient[x][y + 1]
                    r = gradient[x][y - 1]
                break
                case 1:
                    q = gradient[x - 1][y + 1]
                    r = gradient[x + 1][y - 1]
                break
                case 2:
                    q = gradient[x + 1][y]
                    r = gradient[x - 1][y]
                break
                case 3:
                    q = gradient[x + 1][y + 1]
                    r = gradient[x - 1][y - 1]
                break
            }

            let g = gradient[x][y]
            if (g >= q && q >= r) {
                suppression[x][y] = g

                if (lowThreshold > g) {
                    lowThreshold = g
                }

                if (highThreshold < g) {
                    highThreshold = g
                }
            }
        }
    }

    console.log(`Low: ${lowThreshold} High: ${highThreshold}`)

    imgData = imgData.filter([
        function(color, pos) {
            color.r = clamp(mapRange(suppression[pos.x][pos.y], lowThreshold, highThreshold, 0, 1), 0, 1)

            gradiantBuckets[colorToGBIndex(color)] += 1

            return color
        }
    ])

    gradiantBuckets = vecNormalize(gradiantBuckets)
    console.table(gradiantBuckets)
    gradiantBuckets = gradiantBuckets.map(v => {
        if (v > 0.015) return 0
        if (v > 0.00065) return 1
        return 2
    })

    imgData = imgData.filter([
        function(color) {
            let v = gradiantBuckets[colorToGBIndex(color)]
            switch (v) {
                default:
                case 0: v = 0
                break
                case 1: v = 0.5
                break
                case 2: v = 1
                break
            }
            color.r = v
            color.g = v
            color.b = v
            return color
        }
    ])

    function isWeak(color) {
        return color.r > 0.25 && color.r < 0.75
    }

    function isStrong(color) {
        return color.r > 0.75
    }

    function ringTest(dist, pos, next) {
        const d = dist

        dist = dist * 2 + 1
        for (let ox = 0; ox < dist; ox++) {
            for (let oy = 0; oy < dist; oy++) {
                if (ox > 0 && ox < dist - 1 &&
                    oy > 0 && oy < dist - 1) continue

                if (next({
                    x: ox - d + pos.x,
                    y: oy - d + pos.y
                })) return true
            }
        }

        return false
    }

    let totalHysteresisEdges = 0
    let foundConnection
    do {
        foundConnection = false
        imgData = imgData.filter([
            function(color, pos) {
                let result = color.r
        
                if (pos.x > 1 && pos.x < canvas.width - 2 &&
                    pos.y > 1 && pos.y < canvas.height - 2) {
                    if (isWeak(color)) {
                        const test = pNext => isStrong(imgData.getPixel(pNext))
                        if (ringTest(1, pos, test)) {
                            result = 1
                            foundConnection = true
                            totalHysteresisEdges += 1
                        }
                    } else
                    
                    if (isStrong(color)) {
                        result = 1
                    }
                }
        
                color.r = result
                color.g = result
                color.b = result
                
                return color
            }
        ])
    } while (foundConnection)

    console.log(`Hysteresis found ${totalHysteresisEdges} connected edge(s)`)

    imgData = imgData.filter([
        function(color) {
            if (!isStrong(color)) {
                color.r = isWeak(color) * 0.1
                color.g = 0
                color.b = 0
            }
            return color
        }
    ])

    ctx.putImageData(imgData, 0, 0)
    console.log('...done!')

    return canvas
}

window.onload = async () => {
    try {
        const IMG_URL =
            //'./images/test1.png'
            './images/test2.png'
            //'./images/test3.png'


        const img = await loadImage(IMG_URL)
        console.log(`Loaded: ${IMG_URL} [${img.width}x${img.height}]`)

        //load and preprocess image
        const ppCanvas = loadImageData(img)
        const ppCtx = ppCanvas.getContext('2d')

        const ppDbgCanvas = document.getElementById('canvas_pp')
        const ppDbgCtx = ppDbgCanvas.getContext('2d')

        //used for display and any realtime processing
        const canvas = document.getElementById('canvas')
        const ctx = canvas.getContext('2d')

        const hCanvas = document.getElementById('canvas_hough')
        const hCtx = hCanvas.getContext('2d')

        let isDragging = false
        let imgX = 0
        let imgY = 0
        //let imgW = canvas.width
        //let imgH = canvas.height
        //let zoomLevel = 1

        canvas.addEventListener('mousedown', e => {
            isDragging = true
        })
        window.addEventListener('mousemove', e => {
            if (isDragging) {
                imgX -= e.movementX// * zoomLevel
                imgY -= e.movementY// * zoomLevel
            }
        })
        window.addEventListener('mouseup', e => {
            isDragging = false
        })
        window.addEventListener('wheel', e => {
            //zoomLevel += Math.sign(e.deltaY) * (0.25 * zoomLevel)
            //zoomLevel = clamp(zoomLevel, 0.1, 10)
            //imgW = canvas.width * zoomLevel
            //imgH = canvas.height * zoomLevel
        })


        const hDistMax = Math.ceil(Math.hypot(canvas.width, canvas.height))
        const hMaxAngle = 180

        const lkupTheta = createMatrix(hMaxAngle, 2)
        for (let a = 0; a < hMaxAngle; a++) {
            const r = toRadians(a - hMaxAngle / 2)
            lkupTheta[a][0] = Math.cos(r)
            lkupTheta[a][1] = Math.sin(r)
        }

        const hAccumulator = createMatrix(hDistMax * 2, hMaxAngle)
        console.log(`Accum: ${hDistMax * 2}, ${hMaxAngle}`)

        function isEdge(color) {
            return color.r > 0.5
        }

        function render(time) {
            ctx.clearRect(0, 0, canvas.width, canvas.height)

            let ppImgData = ppCtx.getImageData(imgX, imgY, canvas.width, canvas.height)
            ppDbgCtx.putImageData(ppImgData, 0, 0)
            //ctx.putImageData(ppImgData, 0, 0)
            ctx.drawImage(img, -imgX, -imgY)



            //real time processing here.
            let hAccuMax = 0

            for (let d = 0; d < hDistMax * 2; d++) {
                for (let a = 0; a < hMaxAngle; a++) {
                    hAccumulator[d][a] = 0
                }
            }

            ppImgData.filter([
                function(color, pos) {
                    if (isEdge(color)) {
                        for (let a = 0; a < hMaxAngle; a++) {
                            let d = pos.x * lkupTheta[a][0] +
                                    pos.y * lkupTheta[a][1]
                            d = Math.floor(hDistMax - d)
                            hAccumulator[d][a]++
                            if (hAccuMax < hAccumulator[d][a]) {
                                hAccuMax = hAccumulator[d][a]
                            }
                        }
                    }
                    return color
                }
            ])

            let hImgData = hCtx.getImageData(0, 0, hCanvas.width, hCanvas.height)
            hImgData = hImgData.filter([
                function(color, pos) {
                    color.r = hAccumulator[pos.x][pos.y] / hAccuMax
                    color.g = color.r
                    color.b = color.r
                    color.a = 1
                    return color
                }
            ])

            let hbImgData = hImgData.filter([
                filter.blur
            ])

            let hPts = []

            const hActivation = 25 / hAccuMax

            hImgData = hImgData.filter([
                function(color, pos) {
                    if (color.r > hbImgData.getPixel(pos).b + hActivation) {
                        hPts.push({
                            pos,
                            v: color.r
                        })
                    }
                    return color
                }
            ])

            for (let i = 0; i < hPts.length; i++) {
                for (let j = 0; j < hPts.length; j++) {
                    if (i !== j && hPts[j].v > 0 && distance(hPts[i].pos, hPts[j].pos) <= 20) {
                        if (hPts[i].v < hPts[j].v) {
                            hPts[i].v = -1
                        } else {
                            hPts[j].v = -1
                        }
                    }
                }
            }

            hPts = hPts.filter(pt => pt.v > 0)

            hCtx.putImageData(hImgData, 0, 0)
            hCtx.strokeStyle = '#FF0000'
            for (let i = 0; i < hPts.length; i++) {
                let { pos } = hPts[i]
                hCtx.beginPath()
                hCtx.arc(pos.x, pos.y, 10, 0, 2 * Math.PI)
                hCtx.stroke()
            }

            let lSegs = []

            ctx.strokeStyle = '#FF0000'
            ctx.lineWidth = 1

            ctx.beginPath()
            for (let i = 0; i < hPts.length; i++) {
                const { pos } = hPts[i]
                
                const dist = pos.x - hDistMax
                const angle = toRadians(pos.y)

                let ca = Math.cos(angle)
                let sa = Math.sin(angle)

                const b = -dist * sa
                const m =  dist * ca

                ca *= hDistMax
                sa *= hDistMax

                let line = {
                    l1: {
                        x: b + ca,
                        y: m + sa
                    },
                    l2: {
                        x: b - ca,
                        y: m - sa
                    }
                }
                
                ctx.moveTo(line.l1.x, line.l1.y)
                ctx.lineTo(line.l2.x, line.l2.y)

                lSegs.push(line)
            }
            ctx.stroke()

            // line intercept math by Paul Bourke http://paulbourke.net/geometry/pointlineplane/
            // Determine the intersection point of two line segments
            // Return FALSE if the lines don't intersect
            function intersect(x1, y1, x2, y2, x3, y3, x4, y4) {
                // Check if none of the lines are of length 0
                if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) return false
            
                let denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
            
                // Lines are parallel
                if (denominator === 0) return false
            
                let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
                let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator
            
                // is the intersection along the segments
                if (ua < 0 || ua > 1 || ub < 0 || ub > 1) return false
            
                // Return a object with the x and y coordinates of the intersection
                let x = x1 + ua * (x2 - x1)
                let y = y1 + ua * (y2 - y1)
            
                let l1d = vecNormalize(vecSub([x2, y2], [x1, y1]))
                let l2d = vecNormalize(vecSub([x4, y4], [x3, y3]))

                let p = Math.min(
                    Math.abs(vecDot(l1d, l2d)),
                    Math.abs(vecDot(l2d, l1d))
                )

                return {
                    x,
                    y,
                    p
                }
            }

            let iPts = []

            for (let i = 0; i < lSegs.length; i++) {
                let lsi = lSegs[i]
                for (let j = 0; j < lSegs.length; j++) {
                    let lsj = lSegs[j]
                    if (i !== j) {
                        let r = intersect(lsi.l1.x, lsi.l1.y, lsi.l2.x, lsi.l2.y, lsj.l1.x, lsj.l1.y, lsj.l2.x, lsj.l2.y)
                        if (r && r.x >= 0 && r.y >= 0 && r.x < canvas.width && r.y < canvas.height) {
                            iPts.push(r)
                        }
                    }
                }
            }

//*
            for (let i = 0; i < iPts.length; i++) {
                for (let j = 0; j < iPts.length; j++) {
                    if (i !== j && distance(iPts[i], iPts[j]) <= 20) {
                        if (iPts[i].p > iPts[j].p) {
                            iPts[i].p = 2
                        } else {
                            iPts[j].p = 2
                        }
                    }
                }
            }

            iPts = iPts.filter(pt => pt.p <= 0.5)

//*/
            ctx.fillStyle = 'gray'
            ctx.strokeStyle = 'green'
            ctx.lineWidth = 3
            iPts.forEach(p => {
                ctx.beginPath()
                ctx.arc(p.x, p.y, 10, 0, 2 * Math.PI)
                ctx.stroke()
                ctx.fillText(`[${p.p.toFixed(2)}]`, p.x, p.y)
            })
        }

        function animate(time) {
            requestAnimationFrame(animate)
            render(time)
        }
        animate()

    } catch (err) {
        console.log(err)
    }
}