window.addEventListener('load', () => {
    let debug_draw = true

    function offscreen_render(width, height, draw_cb) {
        const canvas = document.createElement('canvas')
        const ctx = canvas.getContext('2d')
        canvas.width = width
        canvas.height = height
        draw_cb(canvas, ctx)
        return canvas
    }

    function async_loop(count, timeout, callback) {
        let i = 0
        const interval = setInterval(() => {
            i < count ? callback(i++) : clearInterval(interval)
        }, timeout)
    }

    const img = document.getElementById('img2')
    const img_size = [img.width, img.height]
    const img_half_size = img_size.divide(2)

    const tree = new NTree(new AABB(img_half_size, img_size), 1)

    function getNearestNode(point, n = 1) {
        const range = new BoundingSphere(point, Math.pow(n, 2))
        
        let nearest_dist = Number.MAX_SAFE_INTEGER
        let nearest_node = null

        tree.query(range).forEach(node => {
            let current_dist = node.boundary.distance(point)
            if (debug_draw) {
                ctx.fillStyle = 'red'
                ctx.fillRect(...node.boundary, 1, 1)
            }
            if (nearest_dist > current_dist) {
                nearest_dist = current_dist
                nearest_node = node
            }
        })

        return nearest_node ? {
            node: nearest_node,
            dist: nearest_dist
        } : getNearestNode(point, ++n)
    }

    function ray_cast(origin, direction) {
        debug_draw && ctx.beginPath()

        let max_itr = 64
        let dist = 0
        let end = origin
        do {
            const result = getNearestNode(origin)

            dist = result.dist
            end = origin

            if (debug_draw) {
                const dir_to_nearest = result.node.boundary.subtract(origin).normalize()
                const ang_to_nearest = Math.atan2(...dir_to_nearest.reverse())
                ctx.moveTo(...origin)
                ctx.arc(...origin, dist, ang_to_nearest, Math.PI * 2 + ang_to_nearest)
            }


            origin = origin.add(direction.multiply(dist))
        } while (dist >= 1 && max_itr-- > 0)

        ctx.strokeStyle = 'green'
        debug_draw && ctx.stroke()

        return end
    }

    const o_canvas = offscreen_render(...img_size, (canvas, ctx) => {
        ctx.drawImage(img, 0, 0)

        const imgData = ctx.getImageData(...canvas.rect())

        for (let y = 0; y < imgData.height; y++) {
            for (let x = 0; x < imgData.width; x++) {
                const clr = imgData.getPixel(x, y)
                if (clr[0] === 0) {
                    if (!tree.insert_point(x, y)) {
                        console.log(`failed @ ${x}, ${y}`)
                    }
                }
            }
        }

        if (debug_draw) {
            for (let y = 0; y < imgData.height; y++) {
                for (let x = 0; x < imgData.width; x++) {
                    const clr = imgData.getPixel(x, y)  
                    if (clr[0] > 50) {
                        imgData.setPixel(x, y, [0, 0, 0, 0])
                    }
                }
            }

            const o_can = offscreen_render(...img_size, (ca, ct) => {
                ct.putImageData(imgData, 0, 0)
            })

            ctx.fillStyle = 'white'
            ctx.fillRect(...canvas.rect())
            tree.render(ctx, 'lightgray')
            ctx.drawImage(o_can, 0, 0)

            const id = ctx.getImageData(...canvas.rect())
            const cnt = img.width * img.height
            let oy = 0
            async_loop(cnt, 1, i => {
                const x = Math.floor(i % img.width)
                const y = Math.floor(i / img.width)

                const clr = id.getPixel(x, y)  
                if (clr[0] > 50) {
                    const { dist } = getNearestNode([x, y])
                    id.setPixel(x, y, [ dist, dist, dist, 255 ])
                } else {
                    id.setPixel(x, y, [ 255, 255, 255, 255 ])
                }
                if (oy != y) {
                    oy = y
                    ctx.putImageData(id, 0, 0)
                }
            })
        }
    })

    let is_mousedown = false
    let pos = [10, 10]
    let dir = [1, 0]

    canvas.addEventListener('mousedown', e => {
        is_mousedown = true
        pos = [e.clientX, e.clientY]
    })

    canvas.addEventListener('mousemove', e => {
        if (is_mousedown) {
            let tmp_dir = [e.clientX, e.clientY].subtract(pos)
            if (tmp_dir.sqr_magnitude() > 0) {
                dir = tmp_dir.normalize()
            }
        }
    })

    canvas.addEventListener('mouseup', e => {
        is_mousedown = false
        let tmp_dir = [e.clientX, e.clientY].subtract(pos)
        if (tmp_dir.sqr_magnitude() > 0) {
            dir = tmp_dir.normalize()
        }
    })

    canvas.addEventListener('mouseleave', e => {
        is_mousedown = false
    })

    const sim = new Simulation((gt, dt) => {
        ctx.fillStyle = 'black'
        ctx.fillRect(...canvas.rect())

        ctx.drawImage(o_canvas, 0, 0)

        let intersect = ray_cast(pos, dir)

        ctx.lineWidth = 2
        ctx.strokeStyle = 'blue'
        ctx.beginPath()
        ctx.moveTo(...pos)
        ctx.lineTo(...intersect)
        ctx.stroke()
    })
})