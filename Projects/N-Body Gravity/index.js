function createBody(max_mass) {
    let m = Math.max(Math.random(), 0.25) * max_mass
    let p = [0, 0].random_normal().multiply(500 * Math.random())
    let v = [0, 0].random_normal().multiply(100 * (max_mass - m))
    return new Body(m, p, v)
}

let bodies = [...Array(30)].map(_ => createBody(10))

let highest_mass_body = 0
let max_mass = 0
for (let [i, b] of bodies.entries()) {
    if (max_mass < b.mass) {
        max_mass = b.mass
        highest_mass_body = i
    }
}

const sim = new Simulation((gt, dt) => {
    dt *= 0.000005

    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    bodies.forEach(body => body.clear_accumulator())

    for (let [i, b0] of bodies.entries()) {
        for (let [j, b1] of bodies.entries()) {
            if (i < j) b0.simulate_gravity(b1)
        }
    }

    bodies.forEach(body => body.update(dt))

    const camera_offset = bodies[highest_mass_body].position.subtract(canvas.size().divide(2))

    bodies.forEach(body => body.render(ctx, camera_offset))
})