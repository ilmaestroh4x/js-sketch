class Body {
    constructor(mass, position = [0, 0], velocity = [0, 0]) {
        this.mass               = mass
        this.position           = position
        this.velocity           = velocity
        this.acceleration       = [0, 0]
        this.force_accumulator  = [0, 0]
    }

    clear_accumulator() {
        for (let i = 0; i < this.force_accumulator.length; i++) {
            this.force_accumulator[i] = 0
        }
    }

    simulate_gravity(other) {
        const diff = other.position.subtract(this.position)
        const dist = Math.max(this.mass, diff.magnitude()) // avoid div by 0

        const K = 10

        let f = (K * this.mass * other.mass) + Math.pow(dist, 2)
            f = diff.multiply(f).divide(dist)

        this.force_accumulator  = this.force_accumulator.add(f)
        other.force_accumulator = other.force_accumulator.subtract(f)
    }

    update(dt) {
        this.acceleration   = this.force_accumulator.divide(this.mass)
        this.velocity       = this.velocity.add(this.acceleration.multiply(dt))
        this.position       = this.position.add(this.velocity.multiply(dt))
    }

    render(ctx, camera_offset) {
        function getHexColor(number){
            return "#"+((number)>>>0).toString(16).slice(-6)
        }
        ctx.fillStyle = getHexColor(this.mass * -25000)
        ctx.beginPath()
        let p = this.position.subtract(camera_offset)
        let r = this.mass
        ctx.arc(p[0], p[1], r, 0, 2 * Math.PI)
        ctx.fill()
    }
}