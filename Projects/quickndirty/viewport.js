class Viewport {
    constructor(clear_color = 'black', auto_resize = true) {
        Viewport.viewports.push(this)

        this.clear_color = clear_color
        this.auto_resize = auto_resize

        this.size = [1, 1]
        this.size_hypot = 1
        this.zoom = 1

        this.scale = 1
        this.pos = [0, 0]
        this.rot = 0

        this.current_ctx = null

        this.aabb = new Rect(0, 0, 1, 1)
    }

    set_size(size) {
        this.size = size.clone()
        this.size_hypot = this.size.magnitude()

        this.aabb.size[0] = this.size_hypot * this.zoom
        this.aabb.size[1] = this.size_hypot * this.zoom
    }

    add_zoom_offset(offset) {
        this.zoom += Math.sign(offset) / 10
        this.zoom = this.zoom.clamp(1, 5)
        this.scale = 1 / this.zoom

        this.aabb.size[0] = this.size_hypot * this.zoom
        this.aabb.size[1] = this.size_hypot * this.zoom
    }

    set_zoom(zoom) {
        this.zoom = zoom
        this.scale = 1 / zoom

        this.aabb.size[0] = this.size_hypot * this.zoom
        this.aabb.size[1] = this.size_hypot * this.zoom
    }

    set_position(position) {
        this.pos = position.clone()
        this.aabb.pos = this.pos.subtract(this.aabb.size.divide(2))
    }

    set_rotation(rotation) {
        this.rot = rotation
    }


    screen_to_world(screen_pos) {
        let viewport_matrix

        if (this.current_ctx == null) {
            viewport_matrix = new DOMMatrix()
        } else {
            viewport_matrix = this.current_ctx.getTransform()
        }

        let transformed_point = new DOMPoint(...screen_pos).matrixTransform(viewport_matrix.invertSelf())

        return [
            transformed_point.x,
            transformed_point.y
        ]
    }

    render(ctx, callback) {
        ctx.clearRect(...canvas.rect())

        ctx.fillStyle = this.clear_color
        ctx.fillRect(...canvas.rect())

        ctx.save()

        this.pos.magnitude()

        ctx.translate(...this.size.divide(2))
        ctx.rotate(this.rot)
        ctx.scale(this.scale, this.scale)
        ctx.translate(...this.pos.multiply(-1))


        Viewport.current = this
        this.current_ctx = ctx

        callback(ctx)

        //this.aabb.render(ctx)

        Viewport.current = null
        this.current_ctx = null
        ctx.restore()
    }
}

Viewport.viewports = []

Viewport.current = null

Viewport.is_rect_visible = rect => {
    let result = Viewport.current && Viewport.current.aabb.intersects(rect)
    //if (!result) console.log('Not visible!')
    return result
}

Viewport.is_point_visible = point => {
    let result = Viewport.current && Viewport.current.aabb.contains_point(point)
    //if (!result) console.log('Not visible!')
    return result
}

window.addEventListener('resize', e => {
    Viewport.viewports.forEach(viewport => {
        if (viewport.auto_resize)
            viewport.set_size([window.innerWidth, window.innerHeight])
    })
})