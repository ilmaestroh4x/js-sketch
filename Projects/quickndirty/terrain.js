const terrian_seed = /*0.03442718035968695*/Math.random()
const simplex = new SimplexNoise(terrian_seed)

function world_generator(...pos) {
    let result = simplex.noise2D(...pos.multiply(0.25))

    result += Math.sin(pos.magnitude() * 3)// - 3

    result += simplex.noise2D(...pos.multiply(0.5))

    //result = pos[1] - 1

    return result.clamp(-1, 1)
}

function cell_generator(pos, size) {
    let vertices = [[0, 0], [0, 1], [1, 1], [1, 0]]
    let corners = vertices.map(vertex => pos.add(vertex.multiply(size)))
    let values = corners.map(corner => world_generator(...corner.multiply(0.5)))

    let path = []
    values.forEach((v0, index, array) => {
        let v1 = array[(index + 1) % array.length]

        if (v0 > 0) {
            path.push(vertices[index])
        }

        if ((v0 > 0) != (v1 > 0)) {
            let t = (0).map_to_range(v0, v1, 0, 1)

            let val0 = vertices[index]
            let val1 = vertices[(index + 1) % array.length]

            path.push(val0.lerp(val1, t))
        }
    })

    return path
}

// right now just used as a wrapper so deep_foreach/map doesn't iterate path
class Terrain_Cell {
    constructor(path) {
        this.path = path
    }
}

class Terrain_Chunk {
    constructor(chunk_offset, chunk_size, block_size) {
        this.chunk_offset = chunk_offset
        this.chunk_size = chunk_size
        this.block_size = block_size

        this.cells = Array.create_matrix(...chunk_size).deep_map((_, pos) => {
            return new Terrain_Cell(cell_generator(chunk_offset.add(pos.divide(chunk_size)), [1, 1].divide(chunk_size)))
        })

        this.cache_canvas = document.createElement('canvas')
        this.cache_ctx = this.cache_canvas.getContext('2d')
        this.cache_ctx.imageSmoothingEnabled = false

        this.is_cache_dirty = true
    }

    get_cell_at(pos) {
        let block_x = Math.floor(pos[0] / this.block_size[0]) - this.chunk_offset[0] * this.chunk_size[0]
        let block_y = Math.floor(pos[1] / this.block_size[1]) - this.chunk_offset[1] * this.chunk_size[1]

        return this.cells[block_x][block_y]
    }

    render(ctx) {
        let size = this.chunk_size.multiply(this.block_size)

        if (this.is_cache_dirty) {
            this.is_cache_dirty = false

            this.cache_canvas.width  = size[0]
            this.cache_canvas.height = size[1]
            
            this.cells.deep_forEach((cell, cell_pos) => {
                this.cache_ctx.save()
                this.cache_ctx.fillStyle = `hsl(${world_generator(...cell_pos.divide(this.chunk_size).add(this.chunk_offset).multiply(0.5)) * 180 + 180}, 100%, 10%)`
                this.cache_ctx.fillRect(...cell_pos.multiply(this.block_size), ...this.block_size)
                this.cache_ctx.beginPath()
                for (let i = 0; i < cell.path.length + 1; i++) {
                    let fn = i == 0 ? 'moveTo' : 'lineTo'
                    let point = cell.path[i % cell.path.length]
                    this.cache_ctx[fn](...cell_pos.add(point).multiply(this.block_size))
                }
                //this.cache_ctx.fill()
                this.cache_ctx.clip()
                this.cache_ctx.drawImage(img_brick, ...cell_pos.multiply(block_size), block_size, block_size)
                this.cache_ctx.restore()
            })
        }

        ctx.save()
        ctx.translate(...this.chunk_offset.multiply(size))
        ctx.drawImage(this.cache_canvas, 0, 0)
        ctx.restore()
    }
}

class Terrain {
    constructor(cache_size, chunk_size, block_size) {
        this.cache_size = cache_size
        this.chunk_size = chunk_size
        this.block_size = block_size

        this.chunk_size_in_pixels = chunk_size * block_size
        this.cache_size_in_pixels = cache_size * chunk_size * block_size

        this.culling_rect = new Rect(0, 0, this.chunk_size_in_pixels, this.chunk_size_in_pixels)
        this.caching_rect = new Rect(0, 0, this.cache_size_in_pixels, this.cache_size_in_pixels)

        this.chunk_queue = []
        this.chunks = {}
    }

    get_cell_collisions(rect) {
        let collisions = []

        let min_x = Math.floor(rect.pos[0] / this.block_size)
        let min_y = Math.floor(rect.pos[1] / this.block_size)
        let max_x = Math.floor((rect.pos[0] + rect.size[0]) / this.block_size)
        let max_y = Math.floor((rect.pos[1] + rect.size[1]) / this.block_size)

        for (let y = min_y; y <= max_y; y++) {
            for (let x = min_x; x <= max_x; x++) {
                let pos = [
                    x * this.block_size,
                    y * this.block_size
                ]
                let cell = this.get_cell_at(pos)
                let isColliding = cell.path.length > 0
                if (isColliding) {
                    collisions.push({
                        path: cell.path.map(v => pos.add(v.multiply(this.block_size))),
                        aabb: new Rect(...pos, this.block_size, this.block_size)
                    })
                }
            }
        }

        return collisions
    }

    get_cell_at(pos) {
        let cache_x = Math.floor(pos[0] / this.cache_size_in_pixels)
        let cache_y = Math.floor(pos[1] / this.cache_size_in_pixels)
        let chunk_x = Math.floor(pos[0] / this.chunk_size_in_pixels)
        let chunk_y = Math.floor(pos[1] / this.chunk_size_in_pixels)

        try {
            return this.chunks[cache_x][cache_y][chunk_x][chunk_y].get_cell_at(pos)
        } catch (err) {
            return new Terrain_Cell([])
        }
    }

    render(ctx, vp_rect = Viewport.current.aabb) {
        stats.chunks_per_frame = 0

        let vp_min_x = vp_rect.pos[0]
        let vp_min_y = vp_rect.pos[1]
        let vp_max_x = vp_rect.pos[0] + vp_rect.size[0]
        let vp_max_y = vp_rect.pos[1] + vp_rect.size[1]


        let min_chunk_x = Math.floor(vp_min_x / this.chunk_size_in_pixels)
        let min_chunk_y = Math.floor(vp_min_y / this.chunk_size_in_pixels)
        let max_chunk_x = Math.floor(vp_max_x / this.chunk_size_in_pixels)
        let max_chunk_y = Math.floor(vp_max_y / this.chunk_size_in_pixels)

        for (let y = min_chunk_y; y <= max_chunk_y; y++) {
            for (let x = min_chunk_x; x <= max_chunk_x; x++) {
                this.culling_rect.pos[0] = x * this.chunk_size_in_pixels
                this.culling_rect.pos[1] = y * this.chunk_size_in_pixels

                if (vp_rect.intersects(this.culling_rect)) {
                    stats.chunks_per_frame++

                    let cache_x = Math.floor(this.culling_rect.pos[0] / this.cache_size_in_pixels)
                    let cache_y = Math.floor(this.culling_rect.pos[1] / this.cache_size_in_pixels)

                    if (!this.chunks[cache_x]) {
                        this.chunks[cache_x] = {}
                    }
                    if (!this.chunks[cache_x][cache_y]) {
                        stats.cached_allocated++
                        this.chunks[cache_x][cache_y] = {}
                    }

                    if (!this.chunks[cache_x][cache_y][x]) {
                        this.chunks[cache_x][cache_y][x] = {}
                    }
                    if (!this.chunks[cache_x][cache_y][x][y]) {
                        stats.chunks_allocated++
                        this.chunks[cache_x][cache_y][x][y] = new Terrain_Chunk(
                            [x, y],
                            [this.chunk_size, this.chunk_size],
                            [this.block_size, this.block_size]
                        )
                        this.chunk_queue.push({cache_x, cache_y, x, y})
                    }

                    if (ctx != null)
                        this.chunks[cache_x][cache_y][x][y].render(ctx)
                }
            }
        }
/*
        while (this.chunk_queue.length > 100) {
            let {cache_x, cache_y, x, y} = this.chunk_queue.shift()
            try {
                delete this.chunks[cache_x][cache_y][x][y]
                stats.chunks_allocated--

                if (Object.keys(this.chunks[cache_x][cache_y]).length == 0) {
                    delete this.chunks[cache_x][cache_y]
                    stats.cached_allocated--
                }
            } catch (err) { }
        }
*/
        /*
        for (let x in this.chunks) {
            for (let y in this.chunks[x]) {
                this.caching_rect.pos[0] = x * this.cache_size_in_pixels
                this.caching_rect.pos[1] = y * this.cache_size_in_pixels
                
                if (!Viewport.is_rect_visible(this.caching_rect)) {
                    //TODO make a better way of cleaning up. (ie. dont clean up until that chunk hasnt been active for n seconds)
                    for (let cx in this.chunks[x][y]) {
                        for (let cy in this.chunks[x][y][cx]) {
                            delete this.chunks[x][y][cx][cy]
                            stats.chunks_allocated--
                        }
                    }

                    delete this.chunks[x][y]
                    stats.cached_allocated--
                }
                else
                {
                    //this.caching_rect.render(ctx, 'blue')
                }
            }
        }
        //*/
    }
}