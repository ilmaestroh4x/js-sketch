class Ball {
    constructor(x, y, vx, vy) {
        this.pos = [x, y]
        this.vel = [vx, vy]

        this.angle = 0
        this.avel = 0

        this.radius = 16

        this.moment_of_inertia = (Math.PI * Math.pow(this.radius.map_to_range(16, 256, 5, 16), 4)) / 4

        this.aabb = new Rect(0, 0, this.radius * 2, this.radius * 2)

        this.collisions = []
        this.is_colliding = false

        this.is_on_ground = false

        this.p = null
    }

    update(dt, gravity) {
        this.vel = this.vel.add(gravity.multiply(dt))
        this.pos = this.pos.add(this.vel.multiply(dt))
        this.vel = this.vel.multiply(0.9999)

        this.angle += this.avel * dt
        this.avel = this.avel * 0.9999


        this.aabb.pos = this.pos.subtract(this.radius)

        this.collisions_c = terrain.get_cell_collisions(this.aabb)


        function nearest_edge_point(point, vertices) {
            let min_dist = Number.MAX_SAFE_INTEGER
            let min_point = null
            for (let i = 0; i < vertices.length + 1; i++) {
                let j = (i + 1) % vertices.length

                let point_on_line = point.project_on_seg(vertices[i], vertices[j])
                let dist = point.distance(point_on_line)
                if (min_dist > dist) {
                    min_dist = dist
                    min_point = point_on_line
                }
            }

            return {
                point: min_point,
                dist: min_dist
            }
        }

        let min_dist = Number.MAX_SAFE_INTEGER
        let min_point = null
        let cell = null
        this.collisions_c.forEach(c => {            
            let { point, dist } = nearest_edge_point(this.pos, c.path)
            if (min_dist > dist) {
                min_dist = dist
                min_point = point
                cell = c
            }
        })

        this.is_colliding = false
        this.is_on_ground = false

        this.resolveCellCollision(cell, min_point, gravity)
    }

    resolveCellCollision(cell, nearest_point, gravity) {
        if (cell == null) return

        let collision_point = nearest_point

        let diff = collision_point.subtract(this.pos)
        let dist = Math.hypot(...diff)

        if (dist >= this.radius) {
            return
        }

        this.is_colliding = true

        if (diff.normalize().dot(gravity.normalize()) > 0.5 && dist <= this.radius) {
            this.is_on_ground = true
        }

        this.p = collision_point

        if (dist == 0) dist = Number.EPSILON
        let norm = diff.divide(dist)
        let speed = this.vel.dot(norm)

        if (speed < 0) {
            return
        }

        let delta = nearest_point.subtract(this.pos)
        let circle_radius = delta.normalize().multiply(this.radius)
        let mtv = delta.subtract(circle_radius).multiply(-1)
        this.pos = this.pos.subtract(mtv)

        let force = norm.multiply(speed * 1.25)//1.5)
        this.vel = this.vel.subtract(force)

        let torque = diff[0] * -this.vel[1] - diff[1] * -this.vel[0]
        this.avel = torque / this.moment_of_inertia
    }

    render(ctx) {
        ctx.save()
        ctx.translate(...this.pos)
        ctx.rotate(this.angle)

        let d = this.radius * 2 * 1.4
        ctx.drawImage(img_pig, -d / 2, -d / 2, d, d)

        ctx.restore()
/*
        if (this.collisions_c instanceof Array)
            this.collisions_c.forEach(cell => {
                ctx.save()
                cell.aabb.render(ctx, 'white')
                ctx.fillStyle = 'white'
                ctx.beginPath()
                for (let i = 0; i < cell.path.length + 1; i++) {
                    let fn = i == 0 ? 'moveTo' : 'lineTo'
                    let point = cell.path[i % cell.path.length]
                    ctx[fn](...point)
                }
                ctx.fill()
                ctx.restore()
            })
*/
    }
}