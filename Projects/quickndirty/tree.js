class Tree {
    constructor() {
        this.depth = 0
        this.parent = null
        this.children = []
        this.data = null
    }

    add_child(child) {
        if (child.parent != null) {
            child.parent.remove_child(child)
        }
        child.depth = this.depth + 1
        child.parent = this
        this.children.push(child)
    }

    remove_child(child) {
        let i = this.children.indexOf(child)
        if (i >= 0) {
            let removed_children = this.children.slice(i, 1)
            removed_children.forEach(c => {
                c.depth = 0
                c.parent = null
            })
        }
    }

    iterate(callback) {
        callback(this)
        this.children.forEach(child => child.iterate(callback))
    }

    clear() {
        this.children.forEach(child => child.clear())
        this.children.length = 0
    }
}