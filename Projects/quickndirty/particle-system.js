class Particle {
    constructor(x, y, vx, vy, rot, vrot, life_span) {
        this.pos = [x, y]
        this.vel = [vx, vy]
        this.rot = rot
        this.vrot = vrot
        this.life_span = life_span
    }

    update(dt, gravity) {
        this.life_span -= dt

        this.vel = this.vel.add(gravity.multiply(dt))
        
        this.pos = this.pos.add(this.vel.multiply(dt))
        this.rot = this.rot + (this.vrot * dt)
    }
}

class ParticleSystem {
    constructor(x, y, vx, vy, vjitter, texture, scale, spawn_rate, life_span) {
        this.inactive = []
        this.particles = []
        this.pos = [x, y]
        this.vel = [vx, vy]
        this.vjitter = vjitter
        this.texture = texture
        this.scale = scale
        this.spawn_rate = spawn_rate
        this.life_span = life_span
        this.time_accumulator = 0
        this.is_running = false
    }

    start() {
        this.is_running = true
    }

    stop() {
        this.is_running = false
    }

    update(dt, gravity = [0, 9.18]) {
        for (let i = this.particles.length; i-- > 0;) {
            if (this.particles[i].life_span <= 0) {
                // remove dead particles
                let p = this.particles.splice(i, 1)[0]
                this.inactive.push(p)
            } else {
                // otherwise update them
                this.particles[i].update(dt, gravity)
            }
        }

        this.time_accumulator += dt
        while (this.time_accumulator > this.spawn_rate) {
            this.time_accumulator -= this.spawn_rate

            let vjvel = [
                (Math.random() * this.vjitter) - (this.vjitter / 2),
                (Math.random() * this.vjitter) - (this.vjitter / 2)
            ]
            if (this.inactive.length > 0) {
                let p = this.inactive.pop()
                p.pos = this.pos
                p.vel = this.vel.add(vjvel)
                p.life_span = this.life_span
                this.particles.push(p)
            } else {
                this.particles.push(new Particle(...this.pos, ...this.vel.add(vjvel), 0, Math.random() * 2 - 1, this.life_span))
            }
        }
    }

    render(ctx) {
        this.particles.forEach(particle => {
            if (Viewport.is_point_visible(particle.pos)) {
                ctx.save()
                ctx.translate(...particle.pos)
                ctx.rotate(particle.rot)

                ctx.globalAlpha = (particle.life_span / this.life_span).clamp(0, 1)
                let sw = this.texture.width * this.scale
                let sh = this.texture.height * this.scale
                ctx.drawImage(this.texture, -sw / 2, -sh / 2, sw, sh)

                ctx.restore()
            }
        })
    }
}
