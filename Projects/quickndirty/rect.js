class Rect {
    constructor(x, y, w, h) {
        this.pos = [x, y]
        this.size = [w, h]
    }

    min() {
        return this.pos.clone()
    }

    max() {
        return this.pos.add(this.size)
    }

    center() {
        return this.pos.add(this.size.divide(2))
    }

    nearest_point(point) {
        return point.clamp(this.min(), this.max())
    }

    nearest_edge_point(point) {
        let corner = [
            [
                this.pos[0],
                this.pos[1]
            ],
            [
                this.pos[0] + this.size[0],
                this.pos[1]
            ],
            [
                this.pos[0] + this.size[0],
                this.pos[1] + this.size[1]
            ],
            [
                this.pos[0],
                this.pos[1] + this.size[1]
            ]
        ]

        // lul
        return [
            point.project_on_seg(corner[0], corner[1]),
            point.project_on_seg(corner[1], corner[2]),
            point.project_on_seg(corner[2], corner[3]),
            point.project_on_seg(corner[3], corner[0]),
        ].map(p => {
            return { p, d: point.distance(p) }
        }).sort((a, b) => {
            return a.d - b.d
        }).map(p => {
            return p.p
        })[0]
    }

    minimum_translation_vector(rect) {
        //TODO fix so gives proper signed vectors
        let x = Math.min(this.pos[0] + this.size[0], rect.pos[0] + rect.size[0]) - Math.max(this.pos[0], rect.pos[0])
        let y = Math.min(this.pos[1] + this.size[1], rect.pos[1] + rect.size[1]) - Math.max(this.pos[1], rect.pos[1])
        return x < y ? [x, 0] : [0, y]
    }

    intersects(rect) {
        return !(
            rect.pos[0] + rect.size[0] <= this.pos[0] ||
            rect.pos[1] + rect.size[1] <= this.pos[1] ||
            this.pos[0] + this.size[0] <= rect.pos[0] ||
            this.pos[1] + this.size[1] <= rect.pos[1]
        )
    }

    contains(rect) {
        return (
            rect.pos[0] + rect.size[0] <= this.pos[0] + this.size[0] &&
            rect.pos[1] + rect.size[1] <= this.pos[1] + this.size[1] &&
            this.pos[0] <= rect.pos[0] &&
            this.pos[1] <= rect.pos[1]
        )
    }

    contains_point(point) {
        return !(
            point[0] < this.pos[0] ||
            point[1] < this.pos[1] ||
            point[0] > this.pos[0] + this.size[0] ||
            point[1] > this.pos[1] + this.size[1]
        )
    }

    render(ctx, color = 'white') {
        ctx.strokeStyle = color
        ctx.strokeRect(...this.pos, ...this.size)

        ctx.fillStyle = 'red'
        ctx.beginPath()
        ctx.arc(...this.center(), 1, 0, Math.PI * 2)
        ctx.fill()
    }
}