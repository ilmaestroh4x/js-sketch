const img_pig = document.getElementById('pig')
const img_money = document.getElementById('money')
const img_brick = document.getElementById('brick')

const canvas = document.getElementById('canvas')
const ctx = canvas.getContext('2d')

ctx.imageSmoothingEnabled = false

canvas.style.cursor = 'none'

let stats = {
    chunks_per_frame: 0,
    chunks_allocated: 0,
    cached_allocated: 0
}

const input_state = {

    left: false,
    right: false,
    up: false,
    down: false,

    space: false,

    get_horizontal_input() {
        let result = 0
        if (this.left)  result -= 1
        if (this.right) result += 1
        return result
    },

    get_vertical_input() {
        let result = 0
        if (this.up)   result -= 1
        if (this.down) result += 1
        return result
    },

    get_input_vector() {
        return [
            this.get_horizontal_input(),
            this.get_vertical_input()
        ].normalize()
    }
}

const block_size = 32   // each block is 32x32 pixels
const chunk_size = 16   // each chunk is 16x16 blocks
const cache_size = 8    // each cache is  8x8  chunks

let viewport = new Viewport()
let terrain = new Terrain(cache_size, chunk_size, block_size)

let mouse_down = false
let mouse_pos  = [0, 0]
let mouse_cell = [0, 0]

let mouse_last_cell = [-1, -1]


canvas.addEventListeners(['mousemove', 'mousedown', 'mouseup', 'mouseleave', 'wheel'], e => {
    let mouse_pos_new = [e.offsetX, e.offsetY]
    let mouse_pos_delta = mouse_pos_new.subtract(mouse_pos)
    mouse_pos = mouse_pos_new

    switch (e.type) {
        case 'mousedown':
            mouse_down = true
            break
        case 'mousemove':
            if (!mouse_last_cell.equals(mouse_cell)) {
                mouse_last_cell = mouse_cell
                //console.log('Cell changed!')
            }
            break
        case 'mouseup':
        case 'mouseleave':
            mouse_down = false
            break
        case 'wheel':
            viewport.add_zoom_offset(e.deltaY)
            break
    }
})

canvas.addEventListeners(['keydown', 'keyup'], e => {
    switch (e.type) {
        case 'keydown':
            if (e.key == 'a') input_state.left  = true
            if (e.key == 'd') input_state.right = true
            if (e.key == 'w') input_state.up    = true
            if (e.key == 's') input_state.down  = true
            if (e.key == ' ') input_state.space = true
            break
        case 'keyup':
            if (e.key == 'a') input_state.left  = false
            if (e.key == 'd') input_state.right = false
            if (e.key == 'w') input_state.up    = false
            if (e.key == 's') input_state.down  = false
            if (e.key == ' ') input_state.space = false
            break
    }
})

let pig_emitter = new ParticleSystem(0, 0, 0, 0, 10, img_money, 1, 0.1, 100)
pig_emitter.start()

let ball = new Ball(0, 0, 0, 1)

/*
function interpolate(p0, p1, p2, t) {
    let s = p0.lerp(p1, t)
    let e = p1.lerp(p2, t)
    return s.lerp(e, t)
}

function renderPath(ctx, gt) {
    let path = [
        [-600, 150],
        [-300, 300],
        [Math.cos(gt * 0.0025) * 300, Math.sin(gt * 0.001) * 500],
        [300, 300],
        [600, 0],
        [700, -500],
    ]

    ctx.strokeStyle = 'red'

    ctx.beginPath()
    for (let i = 0; i < path.length; i++) {
        ctx.lineTo(...path[i])
    }
    ctx.stroke()

    ctx.strokeStyle = 'white'
    ctx.beginPath()

    for (let i = 1; i < path.length - 1; i++) {

        const segments = 20
        for (let j = 0; j < segments; j++) {
            let t = j / (segments - 1)

            let i0 = i - 1
            let i1 = i
            let i2 = i + 1

            let p0 = path[i0]
            let p1 = path[i1]
            let p2 = path[i2]

            if (i0 > 0) {
                p0 = path[i0].lerp(path[i1], 0.5)
            }

            if (i2 < path.length - 1) {
                p2 = path[i1].lerp(path[i2], 0.5)
            }

            let p = interpolate(p0, p1, p2, t)

            ctx.lineTo(...p)
        }
    }

    ctx.stroke()
}
*/
new Simulation((gt, dt) => {
    dt /= 100

    const gravity = ball.pos.normalize().multiply(9.18)
    const gravity_normal = gravity.normalize()
    const perp_gravity_normal = [gravity_normal[1], -gravity_normal[0]]

    // update
    const u0 = performance.now()

    // do input
    let horizontal_step = input_state.get_horizontal_input() * dt * (ball.is_on_ground ? 10 : 3)
    ball.vel[0] += perp_gravity_normal[0] * horizontal_step
    ball.vel[1] += perp_gravity_normal[1] * horizontal_step
    
    if (input_state.space && ball.is_on_ground) {
        let vertical_step = dt * -1000
        ball.vel[0] += gravity_normal[0] * vertical_step
        ball.vel[1] += gravity_normal[1] * vertical_step
    }
    
    // end input

    const steps = Math.min(50, Math.ceil(dt / 0.025))
    const step_size = dt / steps
    for (let step = 0; step < steps; step++) {
        ball.update(step_size, gravity)
    }

    let spawn_rate = (1 / Math.sqrt(ball.vel.magnitude())).map_to_range(0.1, 1, 0.1, 50).clamp(0.1, 50)
    pig_emitter.spawn_rate = spawn_rate
    pig_emitter.pos = ball.pos
    pig_emitter.vel = ball.vel.multiply(-0.1)

    pig_emitter.update(dt, gravity)

    const u1 = performance.now()

    // render
    const r0 = performance.now()

    viewport.set_position(ball.pos)
    viewport.set_rotation(Math.atan2(...gravity_normal))
    //viewport.set_zoom(ball.vel.magnitude() / 100 + 1)

    viewport.render(ctx, ctx => {
        terrain.render(ctx)
        pig_emitter.render(ctx)
        ball.render(ctx)
    })
    const r1 = performance.now()
    
    ctx.font = '14px Arial'
    ctx.fillStyle = 'white'
    ctx.fillText(`chunks_per_frame: ${stats.chunks_per_frame}`, 10, 20)
    ctx.fillText(`chunks_allocated: ${stats.chunks_allocated}`, 10, 40)
    ctx.fillText(`cached_allocated: ${stats.cached_allocated}`, 10, 60)
    ctx.fillText(`update_time(ms): ${(u1 - u0).toFixed(4)}`, 10, 80)
    ctx.fillText(`render_time(ms): ${(r1 - r0).toFixed(4)}`, 10, 100)
})



window.addEventListener('resize', () => {
    canvas.width    = window.innerWidth
    canvas.height   = window.innerHeight
})

// force a resize event to initialize viewports and setup canvas size
window.dispatchEvent(new Event('resize'))