class Demo {
    constructor(size, seed) {
        function generate_test_map(size, seed) {
            const simplex = new SimplexNoise(seed)
            const scale = (3.3 + (seed * 10)) / Math.min(...size)
        
            const center = size.divide(2)
            const d_max = Math.min(...center)
        
            return Array.create_matrix(...size).deep_map((_, position) => {
                const dist_01 = position.distance(center).map_to_range(0, d_max, 1, 0)
                const noise_01 = simplex.noise2D(...position.multiply(scale)).map_to_range(-1, 1, 0, 1)
                return Math.abs(Math.round((dist_01 + noise_01) / 2))
            })
        }

        this.current_pos = [0, 0]
        this.target_pos = [0, 0]
        this.path = []

        this.size = size
        this.seed = seed ? seed : Math.random()
        console.log(`seed:`, this.seed)

        this.map = generate_test_map([1, 1].multiply(this.size), this.seed)
        this.nav_mesh = new NavMesh(this.map)

        this.nav_cell_count = this.nav_mesh.get_nav_cell_count()
        this.nav_rect_count = this.nav_mesh.get_nav_rect_count()
        this.nav_efficiency_ratio = this.nav_rect_count / this.nav_cell_count

        this.map_pixel_scale = 512 / this.size
        this.real_pixel_scale = 1 / this.map_pixel_scale

        // render navmesh visualization to texture for faster redraws
        this.nav_mesh_texture = this.build_nav_mesh_texture()

        this.dt_accumulator = 0
        this.update_speed = (1 / this.size) * 2500 //ms
    }

    mouse_to_demo(mouse_pos) {
        return mouse_pos.divide(this.map_pixel_scale).map(Math.floor)
    }

    pos_is_valid(pos) {
        return pos.comparator([this.size, this.size], (l, r) => l < r).true()
    }

    update_current_pos(current) {
        this.current_pos = current
        this.path = this.nav_mesh.find_simplified_path(this.current_pos, this.target_pos)
    }

    update_target_pos(target) {
        this.target_pos = target
        this.path = this.nav_mesh.find_simplified_path(this.current_pos, this.target_pos)
    }

    resize(win_size) {
        const min_size = Math.min(...win_size)

        this.map_pixel_scale = Math.floor(min_size / this.size)
        this.real_pixel_scale = 1 / this.map_pixel_scale

        this.nav_mesh_texture = this.build_nav_mesh_texture()
    }

    build_nav_mesh_texture() {
        return document.createElement('canvas').offscreen_render(
            this.size * this.map_pixel_scale,
            this.size * this.map_pixel_scale, (_, ctx) => {
            ctx.scale(this.map_pixel_scale, this.map_pixel_scale)
        
            ctx.fillStyle = '#333'
            ctx.fillRect(0, 0, this.size, this.size)
        
            ctx.fillStyle = `black`
            this.nav_mesh.rects.forEach(rect => {
                ctx.fillRect(...rect.pos, ...rect.size)
            })

            ctx.fillStyle = `blue`
            this.nav_mesh.rects.forEach(rect => {
                ctx.fillRect(
                    ...rect.pos.add(this.real_pixel_scale),
                    ...rect.size.subtract(this.real_pixel_scale * 2)
                )
            })
        })
    }

    update(dt) {
        this.dt_accumulator += dt
        if (this.dt_accumulator > this.update_speed) {
            this.dt_accumulator = 0

            // just a little snippit to move the "current pos" along the path
            let block_path = this.nav_mesh.recast_path(this.path)
            if (block_path.length > 1) {
                if (!!this.nav_mesh.raw_map.get_at(block_path[1])) {
                    this.update_current_pos(block_path[1])
                } else {
                    // possible moves
                    let moves = [
                        [ 1,  0],
                        [ 0,  1],
                        [-1,  0],
                        [ 0, -1],
                    ]

                    // translate move to relative space
                    moves = moves.map(move => this.current_pos.add(move))
                    
                    // remove invalid move positions
                    moves = moves.filter(move => {
                        return !!this.nav_mesh.raw_map.get_at(move)
                    })

                    // calc distances to each move for use in later steps
                    const target = this.path[1]
                    moves = moves.map(move => {
                        return {
                            move,
                            dist: move.distance(target)
                        }
                    })

                    // sort by distance to target (prio moves that are closer to target)
                    moves = moves.sort((a, b) => a.dist - b.dist)

                    // move
                    if (!moves.empty()) {
                        this.update_current_pos(moves[0].move)
                    }
                }
            }
        }
    }

    draw(ctx) {
        ctx.lineWidth = this.real_pixel_scale// * 3

        // draw the navmesh texture before scaling (as its already been scaled offscreen)
        ctx.drawImage(this.nav_mesh_texture, 0, 0)

        ctx.save()
        {
            ctx.scale(this.map_pixel_scale, this.map_pixel_scale)
    
            ctx.fillStyle = `#fff5`
            this.nav_mesh.last_simplified_path_explored_rect_ids.forEach(id => {
                let rect = this.nav_mesh.rects[id]
                ctx.fillRect(
                    ...rect.pos.add(this.real_pixel_scale),
                    ...rect.size.subtract(this.real_pixel_scale * 2)
                )
            })

            if (!this.path.empty()) {
                ctx.strokeStyle = `yellow`
                ctx.beginPath()
                this.path.forEach(point => ctx.lineTo(...point.add(0.5)))
                ctx.stroke()
            }

            ctx.fillStyle = `orange`
            ctx.fillRect(...this.current_pos, 1, 1)
            
            ctx.fillStyle = `lime`
            ctx.fillRect(...this.target_pos, 1, 1)
        }
        ctx.restore()
    }
}