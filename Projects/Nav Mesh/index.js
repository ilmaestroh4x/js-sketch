// multi event listener
EventTarget.prototype.addEventListeners = function(types, listener, ...extras) {
    // use set to filter duplicates
    [...new Set(types)].forEach(type => this.addEventListener(type, listener, ...extras))
}

let has_clicked = false
let demo_map_size = 64
let default_seed = 0.6469285262059667
let demo = new Demo(demo_map_size, default_seed)

// really quick and dirty "console" used to print in the bottom left of the visualization
const vconsole = {
    font_size: 16,
    offset: 0,
    log(text) {
        ctx.fillText(text, vconsole.font_size, demo_map_size * demo.map_pixel_scale - (vconsole.font_size * ++vconsole.offset))
    },
    begin() {
        ctx.fillStyle = `white`
        ctx.font = `${vconsole.font_size}px Arial`
    },
    end() {
        vconsole.offset = 0
    }
}

window.addEventListener('keyup', e => {
    console.log(`Processing task...`)
    switch (e.key) {
        case 'Enter':
        case ' ':
            demo = new Demo(demo_map_size)
            break
        case '+':
            demo_map_size = demo_map_size * 2
            demo = new Demo(demo_map_size, demo.seed)
            break
        case '-':
            demo_map_size = demo_map_size / 2
            demo = new Demo(demo_map_size, demo.seed)
            break
    }
    window.dispatchEvent(new Event('resize'))
    console.log(`...completed!`)
})

window.addEventListeners(['mousemove', 'mouseup'], e => {
    let pos = demo.mouse_to_demo([e.offsetX, e.offsetY])
    if (demo.pos_is_valid(pos)) {

        switch (e.type) {
            case 'mousemove':
                demo.update_target_pos(pos)
                break
            case 'mouseup':
                has_clicked = true
                demo.update_current_pos(pos)
                break
        }

        canvas.style.cursor = 'none'
    } else {
        canvas.style.cursor = 'default'
    }
})

window.addEventListener('resize', () => demo.resize([window.innerWidth, window.innerHeight]))
window.dispatchEvent(new Event('resize'))

new Simulation((gt, dt) => {
    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    demo.update(dt)
    demo.draw(ctx)

    vconsole.begin()

    vconsole.log(`Press '-'/'+' to reduce/increse map resolution (can take time to process high res maps.)`)
    vconsole.log(`Press 'Space'/'Enter' to generate a new map & navmesh.`)
    vconsole.log(`Map size: ${demo_map_size}x${demo_map_size} (Cells: ${demo.nav_cell_count}, Rects: ${demo.nav_rect_count}, Reduction: ${((1 - demo.nav_efficiency_ratio) * 100).toFixed(2)}%)`)
    vconsole.log(`Raw path time (avg) = ${demo.nav_mesh.avg_raw_path_time().toFixed(4)}ms, Recast path time (avg) = ${demo.nav_mesh.avg_simplified_time().toFixed(4)}ms`)
    vconsole.log(`Orange: ${demo.current_pos}, Green: ${demo.target_pos}, Yellow: path`)

    if (!has_clicked) {
        vconsole.log(`Click within the blue/black areas to start.`)
    }

    vconsole.end()
})
