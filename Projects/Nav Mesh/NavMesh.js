class NavMesh {
    constructor(map_matrix) {
        const hist_shift = (raw_matrix) => {
            // make a copy of the matrix with all cells being 0 or 1
            let result = raw_matrix.deep_map(value => {
                return !!value ? 1 : 0
            })

            // take the values in each cell and add them to the value of the previous cell.
            result.deep_forEach((value, [x, y]) => {
                if (x > 0 && value > 0) {
                    result.set_at([x, y], result.get_at([x - 1, y]) + 1)
                }
            })

            return result
        }

        const largest_rect_in_hist = (histogram) => {
            let stack = []

            let max_area = 0
        
            let offset = 0
            let size = [0, 0]
        
            let i = 0
            while (i < histogram.length) {
                if (stack.empty() || histogram[stack.peek()] <= histogram[i]) {
                    stack.push(i++)
                } else {
                    let top = stack.pop()
                    let width = stack.empty() ? i : i - stack.peek() - 1
                    let height = histogram[top]
                    let area = height * width
        
                    if (max_area < area) {
                        max_area = area
        
                        offset = i - width
                        size = [width, height]
                    }
                }
            }
        
            while (!stack.empty()) {
                let top = stack.pop()
                let width = stack.empty() ? i : i - stack.peek() - 1
                let height = histogram[top]
                let area = height * width
        
                if (max_area < area) {
                    max_area = area
        
                    offset = i - width
                    size = [width, height]
                }
            }
            
            return { offset, size, area: max_area }
        }

        const largest_rect_in_raw_matrix = (raw_matrix) => {
            const hs_matrix = hist_shift(raw_matrix)

            let result = {
                pos: [0,0],
                size: [0,0],
                area: 0
            }
        
            hs_matrix.forEach((hist, i) => {
                let rect = largest_rect_in_hist(hist)
                if (result.area < rect.area) {
                    result.area = rect.area
                    result.pos = [i - rect.size[1] + 1, rect.offset]
                    result.size = rect.size.reverse()
                }
            })
        
            return result
        }

        const process_cell_in_rect = (matrix, rect, callback = (value, rect, pos) => 0) => {
            return matrix.deep_map((value, pos) => {
                const rect_min = rect.pos
                const rect_max = rect.pos.add(rect.size)

                if (rect_min.comparator(pos, (l, r) => l <= r).true() &&
                    rect_max.comparator(pos, (l, r) => l >  r).true()) {
                    value = callback(value, rect, pos)
                }

                return value
            })
        }

        const cells_to_rects = (raw_matrix) => {
            let result = []

            ;(function fn(rm) {
                const r = largest_rect_in_raw_matrix(rm)
                if (r.area > 0) {
                    rm = process_cell_in_rect(rm, r)

                    // push to result array
                    r.id = result.length
                    r.center = r.pos.add(r.size.divide(2))
                    result.push(r)

                    // find and process next largest rect
                    fn(rm)
                }
            })(raw_matrix.deep_copy()) // use copy
            
            return result
        }

        // stores the raw matrix (cells can be 0 or 1) making up the map
        this.raw_map = map_matrix.deep_copy()

        // stores the rects (from largest to smallest) that cover the map
        this.rects = cells_to_rects(this.raw_map)

        // stores the id matrix (cells values are the index in the rects array that that cell belongs is within)
        this.id_map = map_matrix.deep_map(value => -1) // set all values to -1 to start

        // write cells in id_map to re;ate to the correct rectangle id
        this.rects.forEach(rect => {
            this.id_map = process_cell_in_rect(this.id_map, rect, (value, rect) => rect.id)
        })

        // store all rectangle links (this.links[index_of_rect] = [list of indexes to other connected rects])
        this.links = [...Array(this.rects.length)].map(v => [])
        this.link_connection_points = {}

        const create_link = (id_0, id_1, connection_point) => {
            // make sure link is not pointing to self.
            // and that both ids are valid
            if (id_0 == id_1 || id_0 < 0 || id_1 < 0) return

            // push link id's into keys
            if (!this.links[id_0].includes(id_1)) this.links[id_0].push(id_1)
            if (!this.links[id_1].includes(id_0)) this.links[id_1].push(id_0)

            this.link_connection_points[this.get_connection_key(id_0, id_1)] = connection_point
            this.link_connection_points[this.get_connection_key(id_1, id_0)] = connection_point
        }

        // quick and dirty way of testing for connected rectangles
        this.id_map.deep_forEach((id, pos) => {
            const [x, y] = pos
            if (x > 0) {
                const other_pos = [x - 1, y]
                const other_id = this.id_map.get_at(other_pos)
                create_link(id, other_id, pos.lerp(other_pos, 0.5).add(0.5))
            }
            if (y > 0) {
                const other_pos = [x, y - 1]
                const other_id = this.id_map.get_at(other_pos)
                create_link(id, other_id, pos.lerp(other_pos, 0.5).add(0.5))
            }
        })

        this.last_raw_path_explored_rect_ids = []
        this.last_raw_path_time_max_count = 5
        this.last_raw_path_times = []

        this.last_simplified_path_explored_rect_ids = []
        this.last_simplified_time_max_count = 5
        this.last_simplified_times = []

    }

    avg_raw_path_time() {
        return this.last_raw_path_times.reduce((a, v) => a + v, 0) / this.last_raw_path_times.length
    }

    avg_simplified_time() {
        return this.last_simplified_times.reduce((a, v) => a + v, 0) / this.last_simplified_times.length
    }

    get_nav_cell_count() {
        let result = 0
        this.raw_map.deep_forEach(cell => {
            if (!!cell) result ++
        })
        return result
    }

    get_nav_rect_count() {
        return this.rects.length
    }

    get_connection_key(id_0, id_1) {
        return `${id_0}_${id_1}`
    }

    get_connection_point(id_0, id_1) {
        return this.link_connection_points[this.get_connection_key(id_0, id_1)]
    }

    raycast(start_pos, end_pos) {
        const cast_cells = start_pos.bresenham_line(end_pos, false)

        // loop through cast points
        let last_i = -1
        if (cast_cells.some((point, i) => {
            last_i = i
            return !this.raw_map.get_at(point)
        })) {
            // hit
            return {
                cell: cast_cells[Math.max(0, --last_i)],
                hit: true
            }
        } else {
            // no hit
            return {
                cell: end_pos,
                hit: false
            }
        }
    }

    path_length(path) {
        let result = 0

        for (let i = 1; i < path.length; i++) {
            result += path[i - 1].distance(path[i])
        }

        return result
    }

    recast_path(path) {
        let result = []
        //console.log(`Input:${JSON.stringify(path)}`)

        for (let i = 0; i < path.length; i++) {
            let path_segment
            if (i == 0) {
                path_segment = [ path[i] ]
            } else {
                path_segment = path[i - 1].bresenham_line(path[i], false)
                path_segment.shift() // drop the first element to to prevent doubling up from previous path segment
            }
            //console.log(`Path-Seg:${path_segment}`)
            result.push(...path_segment)
        }

        //console.log(`Output:${JSON.stringify(result)}`)
        return result
    }

    simplify_path(path) {
        let result = []

        // could probably clean up a bit, but this returns the proper result.
        if (!path.empty()) {
            result.push(path[0])

            for (let i = 0; i < path.length; i++) {
                for (let j = i + 1; j < path.length; j++) {
                    if (this.raycast(path[i], path[j]).hit) {
                        result.push(path[--j])
                        i = --j
                        break
                    }
                }
            }

            result.push(path[path.length - 1])
        }

        //console.log(`Output:${JSON.stringify(result)}`)
        return result
    }

    find_simplified_path(start_pos, end_pos) {
        const internal = (start, end) => {
            this.last_simplified_path_explored_rect_ids.length = 0

            let p1 = []
            let p2 = []

            p1 = this.find_raw_path(start, end)
            this.last_simplified_path_explored_rect_ids.push(...this.last_raw_path_explored_rect_ids)

            if (!p1.empty()) {
                p2 = this.find_raw_path(end, start)
                this.last_simplified_path_explored_rect_ids.push(...this.last_raw_path_explored_rect_ids)
            }

            p1 = this.recast_path(p1)
            p2 = this.recast_path(p2)

            p1 = this.simplify_path(p1)
            p2 = this.simplify_path(p2)

            let p1_len = this.path_length(p1)
            let p2_len = this.path_length(p2)

            if (p1_len < p2_len) {
                return p1
            } else {
                p2.reverse()
                return p2
            }
        }

        const t0 = performance.now()
        let result = internal(start_pos, end_pos)
        const tn = performance.now()

        this.last_simplified_times.push(tn - t0)
        if (this.last_simplified_times.length > this.last_simplified_time_max_count) {
            this.last_simplified_times.shift()
        }

        return result
    }

    find_raw_path(start_pos, end_pos) {
        const internal = (start, end) => {
            this.last_raw_path_explored_rect_ids.length = 0

            // default search heuristic
            const heuristic = (pos_0, pos_1) => {
                const D = 1
                const H = D * pos_0.distance(pos_1)
                return H
            }

            const clamp_to_rect = (rect, point) => {
                return point.clamp(rect.pos, rect.pos.add(rect.size, -1))
            }

            // store each nodes (search properties)
            let node_props = this.rects.map((v, i) => {
                return {
                    from: i, // default from id to self.
                    f: 0,
                    g: 0,
                    h: 0,
                    p: null // used to hold position in rect to calculate heuristics and/or distances
                }
            })

            let start_node = this.id_map.get_at(start)
            let end_node = this.id_map.get_at(end)

            if (start_node < 0 || end_node < 0) return [] // no path

            let open_list = []
            let closed_list = []

            open_list.push(start_node)
            node_props[start_node].p = start
            
            while (!open_list.empty()) {

                let current_node = -1

                //find lowest f in openList
                let min_f = Number.POSITIVE_INFINITY
                open_list.forEach(node => {
                    let props = node_props[node]
                    if (min_f > props.f) {
                        min_f = props.f
                        current_node = node
                    }
                })


                // if path found
                if (current_node == end_node) {
                    let path = []

                    let curr = end_node
                    path.push(end)
                    while (curr != start_node) {
                        let curr_n = node_props[curr]

                        let next_p = clamp_to_rect(this.rects[curr_n.from], curr_n.p)
                        let curr_p = clamp_to_rect(this.rects[curr], next_p)

                        path.push(curr_p)
                        path.push(next_p)

                        curr = curr_n.from
                    }
                    path.push(start)

                    path.reverse()

                    return path
                }


                // push node to closed_list
                closed_list.push(current_node)
                this.last_raw_path_explored_rect_ids.push(current_node)

                // remove node from open_list
                let openList_index = open_list.indexOf(current_node)
                if (openList_index >= 0) {
                    open_list.splice(openList_index, 1)
                }

                this.links[current_node].forEach(neighbor_node => {
                    // skip neighbor node if already in closed list.
                    if (closed_list.includes(neighbor_node)) return

                    // use the current nodes 'p'(travel point) to calculate the neighbors 'p'
                    node_props[neighbor_node].p = clamp_to_rect(this.rects[neighbor_node], node_props[current_node].p)
                    let dist = node_props[current_node].p.distance(node_props[neighbor_node].p)

                    let g_score = node_props[current_node].g + dist
                    let is_lowest_g_score = false

                    if (!open_list.includes(neighbor_node)) {
                        is_lowest_g_score = true

                        node_props[neighbor_node].h = heuristic(node_props[neighbor_node].p, end)

                        open_list.push(neighbor_node)

                    } else if (g_score < node_props[neighbor_node].g) {
                        is_lowest_g_score = true
                    }

                    if (is_lowest_g_score) {
                        let neighbor_props = node_props[neighbor_node]
                        neighbor_props.from = current_node
                        neighbor_props.g = g_score
                        neighbor_props.f = g_score + neighbor_props.h
                    }
                })
            }

            return [] // no path found...
        }

        const t0 = performance.now()
        let result = internal(start_pos, end_pos)
        const tn = performance.now()

        this.last_raw_path_times.push(tn - t0)
        if (this.last_raw_path_times.length > this.last_raw_path_time_max_count) {
            this.last_raw_path_times.shift()
        }

        return result
    }
}