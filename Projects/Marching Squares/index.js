function offscreen_render(width, height, draw_cb, canvas = document.createElement('canvas')) {
    const ctx = canvas.getContext('2d')
    canvas.width = width
    canvas.height = height
    draw_cb(canvas, ctx)
    return canvas
}

function debounce(fn, time) {
    let timeout
    return function() {
        const functionCall = () => fn.apply(this, arguments)
        clearTimeout(timeout)
        timeout = setTimeout(functionCall, time)
    }
}

const shapes = [
    /*  0 */[],
    /*  1 */[[0.5, 1.0], [0.0, 1.0], [0.0, 0.5]],
    /*  2 */[[1.0, 0.5], [1.0, 1.0], [0.5, 1.0]],
    /*  3 */[[0.0, 0.5], [1.0, 0.5], [1.0, 1.0], [0.0, 1.0]],
    /*  4 */[[0.5, 0.0], [1.0, 0.0], [1.0, 0.5]],
    /*  5 */[[0.5, 0.0], [1.0, 0.0], [1.0, 0.5], [0.5, 1.0], [0.0, 1.0], [0.0, 0.5]],
    /*  6 */[[0.5, 0.0], [1.0, 0.0], [1.0, 1.0], [0.5, 1.0]],
    /*  7 */[[0.5, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0], [0.0, 0.5]],
    /*  8 */[[0.0, 0.0], [0.5, 0.0], [0.0, 0.5]],
    /*  9 */[[0.0, 0.0], [0.5, 0.0], [0.5, 1.0], [0.0, 1.0]],
    /* 10 */[[0.0, 0.0], [0.5, 0.0], [1.0, 0.5], [1.0, 1.0], [0.5, 1.0], [0.0, 0.5]],
    /* 11 */[[0.0, 0.0], [0.5, 0.0], [1.0, 0.5], [1.0, 1.0], [0.0, 1.0]],
    /* 12 */[[0.0, 0.0], [1.0, 0.0], [1.0, 0.5], [0.0, 0.5]],
    /* 13 */[[0.0, 0.0], [1.0, 0.0], [1.0, 0.5], [0.5, 1.0], [0.0, 1.0]],
    /* 14 */[[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.5, 1.0], [0.0, 0.5]],
    /* 15 */[[0.0, 0.0], [1.0, 0.0], [1.0, 1.0], [0.0, 1.0]],
]

let simplex = new SimplexNoise()

const test = new MarchingGenerator((...pos) => {
    const v = simplex.noise2D(...pos)
    return Math.round(v * 0.5 + 0.5)
}, 2)

const c_w = 1024
const c_h = 512
const w = 32
const h = 16
const noise_scale = 0.1
const m = test.generate([0, 0], [w, h], noise_scale)

const tile_size = [c_w / w, c_h / h]

let hover_tile = [-1, -1]

canvas.addEventListener('mouseup', e => {
    if (hover_tile[0] < w && hover_tile[1] < h) {
        m[hover_tile[0]][hover_tile[1]] ^= true
        window.dispatchEvent(new Event('resize'))
    }
})
canvas.addEventListener('mousemove', e => {
    hover_tile = [e.clientX, e.clientY].divide(tile_size).map(Math.floor)
})


function draw_hover_tile(ctx) {
    const p = hover_tile.multiply(tile_size)
    ctx.strokeStyle = 'black'
    ctx.strokeRect(...p.add(0.5), ...tile_size.subtract(1))
    ctx.strokeStyle = 'white'
    ctx.strokeRect(...p.add(1.5), ...tile_size.subtract(3))
}

let noise_view_canvas
let point_view_canvas
let shape_view_canvas
let tex2d_view_canvas

window.addEventListener('resize', () => {
    noise_view_canvas = offscreen_render(w, h, (canvas, ctx) => {
        const imgdata = ctx.createImageData(w, h)

        for (let y = 0; y < h; y++) {
            for (let x = 0; x < w; x++) {
                const c = m[x][y] * 255
                imgdata.setPixel(x, y, [c, c, c, 255])
            }
        }

        ctx.putImageData(imgdata, 0, 0)
    }, noise_view_canvas)

    point_view_canvas = offscreen_render(c_w, c_h, (canvas, ctx) => {
        ctx.clearRect(...canvas.rect())

        const scale = canvas.size().divide([w, h])

        ctx.fillStyle = 'green'
        ctx.beginPath()
        for (let y = 0; y < h; y++) {
            for (let x = 0; x < w; x++) {
                if (!m[x][y]) {
                    const pos = [x, y].add(0.5).multiply(scale).map(Math.round).subtract(1)
                    ctx.moveTo(...pos)
                    ctx.rect(...pos, 3, 3)
                }
            }
        }
        ctx.fill()
        
        ctx.fillStyle = 'red'
        ctx.beginPath()
        for (let y = 0; y < h; y++) {
            for (let x = 0; x < w; x++) {
                if (m[x][y]) {
                    const pos = [x, y].add(0.5).multiply(scale).map(Math.round).subtract(1)
                    ctx.moveTo(...pos)
                    ctx.rect(...pos, 3, 3)
                }
            }
        }
        ctx.fill()
    }, point_view_canvas)

    shape_view_canvas = offscreen_render(c_w, c_h, (canvas, ctx) => {
        ctx.clearRect(...canvas.rect())

        const scale = canvas.size().divide([w, h])

        ctx.strokeStyle = '#00F1'
        for (let y = 0; y < h - 1; y++) {
            for (let x = 0; x < w - 1; x++) {
                //1 << 3 | 1 << 2 | 1 << 1 | 1
                const shape_id =
                    m[x + 0][y + 0] << 3 |
                    m[x + 1][y + 0] << 2 |
                    m[x + 1][y + 1] << 1 |
                    m[x + 0][y + 1] << 0

                //
                const shape_path = shapes[shape_id]

                const world_pos = [x, y].add(0.5).multiply(scale)

                if (shape_path.length > 0) {
                    ctx.beginPath()
                    for (let i = 0; i < shape_path.length + 1; i++) {
                        let vertex = shape_path[i % shape_path.length]
                            vertex = vertex.multiply(scale)
                            vertex = vertex.add(world_pos)

                            // clean up render lines
                            vertex = vertex.map(Math.round)
                            vertex = vertex.add(0.5)
                        
                        ctx[i == 0 ? 'moveTo' : 'lineTo'](...vertex)
                    }
                    ctx.stroke()
                }
            }
        }

    }, shape_view_canvas)

    tex2d_view_canvas = offscreen_render(c_w, c_h, (canvas, ctx) => {
        ctx.clearRect(...canvas.rect())

        const texture = document.getElementById('tex2d')

        const scale = canvas.size().divide([w, h])

        const src_width  = [texture.width, texture.height].divide(4)
        const dst_width  = scale


        for (let y = 0; y < h - 1; y++) {
            for (let x = 0; x < w - 1; x++) {
                //1 << 3 | 1 << 2 | 1 << 1 | 1
                const shape_id =
                    m[x + 0][y + 0] << 3 |
                    m[x + 1][y + 0] << 2 |
                    m[x + 1][y + 1] << 1 |
                    m[x + 0][y + 1] << 0

                let src_offset = [Math.floor(shape_id / 4), Math.floor(shape_id % 4)]
                    src_offset = src_offset.multiply(src_width)

                let dst_offset = [x, y]
                    dst_offset = dst_offset.add(0.5)
                    dst_offset = dst_offset.multiply(scale)

                ctx.imageSmoothingEnabled = false
                //ctx.drawImage(texture, ...src_offset.add(1), ...src_width.subtract(2), ...dst_offset, ...dst_width)
                ctx.drawImage(texture, ...src_offset, ...src_width, ...dst_offset, ...dst_width)
                
            }
        }

    }, tex2d_view_canvas)
})
window.dispatchEvent(new Event('resize'))

const sim = new Simulation((gt, dt) => {
    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())
/*
    if (noise_view_canvas) {
        ctx.drawImage(noise_view_canvas, ...canvas.rect())
    }

    if (point_view_canvas) {
        ctx.drawImage(point_view_canvas, ...canvas.rect())
    }
//*/

    if (tex2d_view_canvas) {
        ctx.drawImage(tex2d_view_canvas, 0, 0)
    }
    if (shape_view_canvas) {
        //ctx.drawImage(shape_view_canvas, 0, 0)
    }

    
    draw_hover_tile(ctx)
})