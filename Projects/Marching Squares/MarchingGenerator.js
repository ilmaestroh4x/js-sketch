class MarchingGenerator {
    constructor(generator_function, dimensions = 2) {
        if (typeof generator_function !== 'function') {
            throw Error(`Param: 'generator_function' must be a function.`)
        }

        this.gen_fn = generator_function
        this.dims = dimensions
    }

    generate(offset, chunk_size, scale) {
        if (offset.length != this.dims || chunk_size.length != this.dims) {
            throw Error(`Error: 'MarchingGenerator.generate' offset and chunk_size must be same length as dims.`)
        }

        return Array.create_matrix(...chunk_size).deep_map((e, indices) => {
            return this.gen_fn(...offset.add(indices.multiply(scale)))
        })
    }
}