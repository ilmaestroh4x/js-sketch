function structAABB(min, max) {
    return { min, max }
}

function structCircle(position, radius) {
    return { position, radius }
}

function structManifold(a, b, penetration, normal) {
    return { a, b, penetration, normal }
}

function AABBvsAABB(a, b) {
  // Exit with no intersection if found separated along an axis
  if (a.max[0] < b.min[0] || a.min[0] > b.max[0]) return false
  if (a.max[1] < b.min[1] || a.min[1] > b.max[1]) return false
 
  // No separating axis found, therefor there is at least one overlapping axis
  return true
}

function CirclevsCircle(a, b) {
    return (
        Math.pow(a.position[0] + b.position[0], 2) +
        Math.pow(a.position[1] + b.position[1], 2)
    ) > Math.pow(a.radius + b.radius, 2)
}

function ResolveCollision(a, b) {
    // Calculate relative velocity
    let rv = b.velocity.subtract(a.velocity)

    // Calculate relative velocity in terms of the normal direction
    let velAlongNormal = rv.dot(normal)

    // Do not resolve if velocities are separating
    if (velAlongNormal > 0) return

    // Calculate restitution
    let e = Math.min(a.restitution, b.restitution)

    // Calculate impulse scalar
    let j = -(1 + e) * velAlongNormal
    j /= a.inv_mass + b.inv_mass

    // Apply impulse
    let impulse = normal.multiply(j)
    a.velocity -= a.inv_mass * impulse
    b.velocity += b.inv_mass * impulse
}

function PositionalCorrection(a, b) {
    const percent = 0.2 // usually 20% to 80%
    const k_slop = 0.01 // usually 0.01 to 0.1
    let correction = Math.max(penetration - k_slop, 0) / (a.inv_mass + b.inv_mass) * percent * n
    a.position -= a.inv_mass * correction
    b.position += b.inv_mass * correction
}

function CirclevsCircle(m) {
    // Setup a couple pointers to each object
    let a = m.a
    let b = m.b

    // Vector from A to B
    let n = b.pos.subtract(a.pos)

    let r = Math.pow(a.radius + b.radius, 2)

    if (n.sqr_magnitude() > r) return false

    // Circles have collided, now compute manifold
    let d = n.Length() // perform actual sqrt

    // If distance between circles is not zero
    if (d != 0) {
        // Distance is difference between radius and distance
        m.penetration = r - d

        // Utilize our d since we performed sqrt on it already within Length( )
        // Points from A to B, and is a unit vector
        c.normal = t / d
        return true
    }

    // Circles are on same position
    else {
        // Choose random (but consistent) values
        c.penetration = a.radius
        c.normal = [1, 0]
        return true
    }
}

function AABBvsAABB(m) {
    // Setup a couple pointers to each object
    let a = m.a
    let b = m.b

    // Vector from A to B
    let n = b.pos.subtract(a.pos)

    let abox = a.aabb
    let bbox = b.aabb

    // Calculate half extents along x axis for each object
    let a_extent = (abox.max.x - abox.min.x) / 2
    let b_extent = (bbox.max.x - bbox.min.x) / 2

    // Calculate overlap on x axis
    let x_overlap = a_extent + b_extent - Math.abs(n.x)

    // SAT test on x axis
    if (x_overlap > 0) {
        // Calculate half extents along x axis for each object
        let a_extent = (abox.max.y - abox.min.y) / 2
        let b_extent = (bbox.max.y - bbox.min.y) / 2

        // Calculate overlap on y axis
        let y_overlap = a_extent + b_extent - Math.abs(n.y)

        // SAT test on y axis
        if (y_overlap > 0) {
            // Find out which axis is axis of least penetration
            if (x_overlap > y_overlap) {
                // Point towards B knowing that n points from A to B
                if (n.x < 0)
                    m.normal = [-1, 0]
                else
                    m.normal = [ 0, 0]

                m.penetration = x_overlap
                return true
            } else {
                // Point toward B knowing that n points from A to B
                if (n.y < 0)
                    m.normal = [0, -1]
                else
                    m.normal = [0,  1]

                m.penetration = y_overlap
                return true
            }
        }
    }
}

function AABBvsCircle(m) {
    // Setup a couple pointers to each object
    let a = m.a
    let b = m.b

    // Vector from A to B
    let n = b.pos.subtract(a.pos)

    // Closest point on A to center of B
    let closest = n

    // Calculate half extents along each axis
    let x_extent = (a.aabb.max.x - a.aabb.min.x) / 2
    let y_extent = (a.aabb.max.y - a.aabb.min.y) / 2

    // Clamp point to edges of the AABB
    closest.x = closest.x.clamp(-x_extent, x_extent)
    closest.y = closest.y.clamp(-y_extent, y_extent)

    let inside = false

    // Circle is inside the AABB, so we need to clamp the circle's center
    // to the closest edge
    if (n == closest) {
        inside = true

        // Find closest axis
        if (Math.abs(n.x) > Math.abs(n.y)) {
            // Clamp to closest extent
            if (closest.x > 0)
                closest.x =  x_extent
            else
                closest.x = -x_extent
        }

        // y axis is shorter
        else {
            // Clamp to closest extent
            if (closest.y > 0) {
                closest.y =  y_extent
            } else {
                closest.y = -y_extent
            }
        }
    }

    let normal = n - closest
    let d = normal.sqr_magnitude()
    let r = b.radius

    // Early out of the radius is shorter than distance to closest point and
    // Circle not inside the AABB
    if (d > r * r && !inside) {
        return false
    }

    // Avoided sqrt until we needed
    d = Math.sqrt(d)

    // Collision normal needs to be flipped to point outside if circle was
    // inside the AABB
    if (inside) {
        m.normal = -n
    } else {
        m.normal =  n
    }

    m.penetration = r - d

    return true
}

class Material {
    constructor() {
/*
    Rock       Density : 0.6  Restitution : 0.1
    Wood       Density : 0.3  Restitution : 0.2
    Metal      Density : 1.2  Restitution : 0.05
    BouncyBall Density : 0.3  Restitution : 0.8
    SuperBall  Density : 0.3  Restitution : 0.95
    Pillow     Density : 0.1  Restitution : 0.2
    Static     Density : 0.0  Restitution : 0.4
*/
        this.density = 0.6
        this.restitution = 0.1
    }
}

class MassData {
    constructor() {
        this.mass = 0
        this.inv_mass = 0

        // For rotations
        this.inertia = 0
        this.inverse_inertia = 0
    }
}

class body {
    constructor(shape_index) {
        this.shape = shape_index
        //Transform tx
        this.material = new Material()
        this.mass_data = new MassData()
        this.velocity = [0, 0]
        this.force = [0, 0]
        this.gravityScale = 0
    }
}




/*
    const vert_count = Math.round(Math.random() * 5 + 3)
    const verts = new Array(vert_count)

    const step = (Math.PI * 2) / vert_count
    for (let i = 0; i < vert_count; i++) {
        const angle = i * step
        verts[i] = [
            Math.cos(angle),
            Math.sin(angle)
        ].multiply(128)
    }

    let test_image = document.createElement('canvas').offscreen_render(256, 256, (canvas, ctx) => {
        console.log(canvas, ctx)
        ctx.fillStyle = 'blue'
        ctx.fillRect(...canvas.rect())

        ctx.strokeStyle = 'red'
        ctx.beginPath()
        for (let i = 0; i < verts.length + 1; i++) {
            let vertex = verts[i % verts.length]
                vertex = vertex.add(128)

                // clean up render lines
                vertex = vertex.map(Math.round)
                vertex = vertex.add(0.5)
            
            ctx[i == 0 ? 'moveTo' : 'lineTo'](...vertex)
        }
        ctx.stroke()
    })

    // save the unrotated context of the canvas so we can restore it later
    // the alternative is to untranslate & unrotate after drawing
    ctx.save()
    
    // move to the center of the canvas
    ctx.translate(...canvas.size().divide(2))

    // rotate the canvas to the specified degrees
    const degrees = gt * 0.1
    ctx.rotate(degrees * Math.PI / 180)

    // draw the image
    // since the context is rotated, the image will be rotated also
    ctx.drawImage(test_image, ...test_image.size().divide(-2))

    ctx.strokeStyle = 'lime'
    ctx.beginPath()
    for (let i = 0; i < verts.length + 1; i++) {
        let vertex = verts[i % verts.length]

            // clean up render lines
            vertex = vertex.map(Math.round)
            vertex = vertex.add(0.5)
        
        ctx[i == 0 ? 'moveTo' : 'lineTo'](...vertex)
    }
    ctx.stroke()

    // we’re done with the rotating so restore the unrotated context
    ctx.restore()
//*/