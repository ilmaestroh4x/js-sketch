//https://www.toptal.com/game/video-game-physics-part-i-an-introduction-to-rigid-body-dynamics

const NUM_RIGID_BODIES = 10

// 2D box shape. Physics engines usually have a couple different classes of shapes
// such as circles, spheres (3D), cylinders, capsules, polygons, polyhedrons (3D)...
class BoxShape {
    constructor() {
        this.width = 0
        this.height = 0
        this.mass = 0
        this.momentOfInertia = 0
    }

    // Calculates the inertia of a box shape and stores it in the momentOfInertia variable.
    CalculateBoxInertia() {
        let m = this.mass
        let w = this.width
        let h = this.height
        this.momentOfInertia = m * (w * w + h * h) / 12
    }
}

/*

force = [?,?]
r = [?,?] // r is the 'arm vector' that goes from the center of mass to the point of force application
torque = r.x * force.y - r.y * force.x

*/


/*

linear_acceleration = rigid_body_force / mass
linear_velocity += linear_acceleration * delta_time
position += linear_velocity * delta_time

angular_acceleration = torque / moment_of_inertia
angular_velocity += angular_acceleration * delta_time
angle += angular_velocity * delta_time

*/


// Two dimensional rigid body
class RigidBody{
    constructor() {
        this.force = [0, 0]
        this.linearVelocity = [0, 0]
        this.position = [0, 0]
        this.angle = 0
        this.angularVelocity = 0
        this.torque = 0
        this.shape = new BoxShape()
    }
}


// Global array of rigid bodies.
const rigidBodies = [...Array(NUM_RIGID_BODIES)].map(() => new RigidBody())

// Initializes rigid bodies with random positions and angles and zero linear and angular velocities.
// They're all initialized with a box shape of random dimensions.
//InitializeRigidBodies()
for (let i = 0; i < NUM_RIGID_BODIES; ++i) {
    let rigidBody = rigidBodies[i]
    rigidBody.position = [Math.random() * 500, Math.random() * 50]
    rigidBody.angle = (Math.random() * 360) / 360 * Math.PI * 2
    rigidBody.linearVelocity = [0, 0]
    rigidBody.angularVelocity = 0
    
    let shape = new BoxShape()
    shape.mass   = 1 + (Math.random() * 10)
    shape.width  = 1 + (Math.random() * 2)
    shape.height = 1 + (Math.random() * 2)

    shape.CalculateBoxInertia()
    
    rigidBody.shape = shape
}


// Prints the position and angle of each body on the output.
// We could instead draw them on screen.
function RenderRigidBodies() {
    for (let i = 0; i < NUM_RIGID_BODIES; ++i) {
        let rigidBody = rigidBodies[i]
        //console.log(`body[${i}] p = (${rigidBody.position[0]}, ${rigidBody.position[1]}), a = ${rigidBody.angle}`)

        // save the unrotated context of the canvas so we can restore it later
        // the alternative is to untranslate & unrotate after drawing
        ctx.save()
        
        // move to the center of the canvas
        ctx.translate(rigidBody.position[0], rigidBody.position[1])

        // rotate the canvas to the specified degrees
        const degrees = rigidBody.angle
        ctx.rotate(degrees * Math.PI / 180)

        // draw the image
        // since the context is rotated, the image will be rotated also
        ctx.fillStyle = 'magenta'
        ctx.fillRect(
            -(rigidBody.shape.width  * 10) / 2,
            -(rigidBody.shape.height * 10) / 2,
            rigidBody.shape.width  * 10,
            rigidBody.shape.height * 10
        )

        // we’re done with the rotating so restore the unrotated context
        ctx.restore()
    }
}

new Simulation((gt, dt) => {
    dt *= 0.01

    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    RenderRigidBodies()

    for (let i = 0; i < NUM_RIGID_BODIES; ++i) {
        let rigidBody = rigidBodies[i]


//ComputeForceAndTorque(rigidBody);
        let f = [0, 10]
        rigidBody.force = f
    
        // r is the 'arm vector' that goes from the center of mass to the point of force application
        let r = [
            rigidBody.shape.width  / 2,
            rigidBody.shape.height / 2
        ]
        rigidBody.torque = r[0] * f[1] - r[1] * f[0]
//

        let linearAcceleration = [rigidBody.force[0] / rigidBody.shape.mass, rigidBody.force[1] / rigidBody.shape.mass]
        rigidBody.linearVelocity[0] += linearAcceleration[0] * dt;
        rigidBody.linearVelocity[1] += linearAcceleration[1] * dt;
        rigidBody.position[0] += rigidBody.linearVelocity[0] * dt;
        rigidBody.position[1] += rigidBody.linearVelocity[1] * dt;

        let angularAcceleration = rigidBody.torque / rigidBody.shape.momentOfInertia;
        rigidBody.angularVelocity += angularAcceleration * dt;
        rigidBody.angle += rigidBody.angularVelocity * dt;
    }
})