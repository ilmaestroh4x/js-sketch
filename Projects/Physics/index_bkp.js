class BoxShape {
    constructor(width, height, mass) {
        this.width = width
        this.height = height
        this.mass = mass
        this.moment_of_inertia = this.CalculateBoxInertia()
    }

    // Calculates the inertia of a box shape and stores it in the momentOfInertia variable.
    CalculateBoxInertia() {
        let m = this.mass
        let w = this.width
        let h = this.height
        return m * (w * w + h * h) / 12
    }

    draw(ctx) {
        ctx.fillStyle = 'magenta'
        ctx.fillRect(
            -this.width  / 2,
            -this.height / 2,
            this.width,
            this.height
        )
    }
}

class RigidBody {
    constructor(position, shape) {
        this.shape = shape

        this.force = [0, 0]
        this.torque = 0

        this.linear_acceleration = [0, 0]
        this.linear_velocity = [0, 0]
        this.position = position
        
        this.angular_acceleration = 0
        this.angular_velocity = 0
        this.angle = 0

        this.forces = []
    }

    addForce(force, position) {
        this.forces.push({ force, position })

        if (position) {
            // r is the 'arm vector' that goes from the center of mass to the point of force application
            let r = position.subtract(this.position)
            let t = r[0] * force[1] - r[1] * force[0]
            
            this.torque += t
        }

        this.force = this.force.add(force)
    }

    update(dt) {
        this.linear_acceleration = this.force.divide(this.shape.mass)
        this.linear_velocity = this.linear_velocity.add(this.linear_acceleration.multiply(dt))
        this.position = this.position.add(this.linear_velocity.multiply(dt))

        this.angular_acceleration = this.torque / this.shape.moment_of_inertia
        this.angular_velocity += this.angular_acceleration * dt
        this.angle += this.angular_velocity * dt

        this.force = [0, 0]
        this.torque = 0
    }

    draw(ctx) {
        ctx.save()
        ctx.translate(...this.position)
        ctx.rotate(this.angle * Math.PI / 180)
        this.shape.draw(ctx)
        ctx.restore()

        this.forces.forEach(({force, position}) => {
            if (position) {
                ctx.strokeStyle = 'green'
                ctx.beginPath()
                ctx.arc(...position, 3, 0, Math.PI * 2)
                ctx.moveTo(...position)
                ctx.lineTo(...position.add(force))
                ctx.stroke()
            } else {
                ctx.strokeStyle = 'yellow'
                ctx.beginPath()
                ctx.arc(...this.position, 3, 0, Math.PI * 2)
                ctx.moveTo(...this.position)
                ctx.lineTo(...this.position.add(force))
                ctx.stroke()
            }
        })
        this.forces.length = 0
    }
}

const rigidbody = new RigidBody(canvas.size().divide(2), new BoxShape(100, 10, 1))


let mouse_position = [100, 100]
canvas.addEventListener('mousemove', e => {
    mouse_position = [e.clientX, e.clientY]
})

new Simulation((gt, dt) => {
    dt *= 0.01

    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    rigidbody.addForce([0, 50])
    rigidbody.addForce([0,-50], [mouse_position[0], rigidbody.position[1]])

    rigidbody.update(dt)

    rigidbody.draw(ctx)
})