function rotate_point_2d(point, angle) {
    const c = Math.cos(angle)
    const s = Math.sin(angle)
    return [
        point[0] * c - point[1] * s,
        point[0] * s + point[1] * c
    ]
}

function AABBOverlap(aabb0, aabb1) {
    const d0 = aabb1.min.subtract(aabb0.max)
    if (d0.some(v => v > 0)) return false
    const d1 = aabb0.min.subtract(aabb1.max)
    if (d1.some(v => v > 0)) return false
    return true
}

function SAT(rb0, rb1) {
    if (AABBOverlap(rb0.collider.aabb, rb1.collider.aabb)) {
        let min_overlap = Number.MAX_SAFE_INTEGER
        let mtv = null

        ;[rb0, rb1].some(rb => {
            return rb.collider.normals.some(axis => {
                const c0mm = rb0.collider.projectToAxis(axis)
                const c1mm = rb1.collider.projectToAxis(axis)
        
                const overlap = Math.min(c0mm.max, c1mm.max) - Math.max(c0mm.min, c1mm.min)
                if (min_overlap > overlap) {
                    min_overlap = overlap
                    mtv = axis
                }
                
                return min_overlap < 0
            })
        })

        if (min_overlap > 0) {
            const overlap = rb1.position.subtract(rb0.position).normalize()
            mtv = mtv.multiply(Math.sign(overlap.dot(mtv)))

            return {
                rigidbody1,
                rigidbody2,
                mtv,
                overlap: min_overlap
            }
        }
    }
    return null
}

function ContactPoint(collision) {
    const rb0 = collision.rigidbody1
    const rb1 = collision.rigidbody2

    const rb0dir = collision.mtv.multiply( 1)
    const rb1dir = collision.mtv.multiply(-1)

    const rb0faces = rb0.collider.getCulledColliderFaces(rb0dir)
    const rb1faces = rb1.collider.getCulledColliderFaces(rb1dir)
    
    ctx.strokeStyle = '#fff6'
    ctx.beginPath()
    rb0faces.edges.forEach(([i0, i1]) => {
        ctx.moveTo(...rb0faces.vertices[i0])
        ctx.lineTo(...rb0faces.vertices[i1])
    })
    rb1faces.edges.forEach(([i0, i1]) => {
        ctx.moveTo(...rb1faces.vertices[i0])
        ctx.lineTo(...rb1faces.vertices[i1])
    })
    ctx.stroke()
//*
    ctx.strokeStyle = 'orange'
    ctx.beginPath()
    rb0faces.vertices.forEach(vertex => {
        rb1faces.edges.forEach(([i0, i1]) => {
            let proj = vertex.project_on_seg(rb1faces.vertices[i0], rb1faces.vertices[i1], true, true)
            if (proj && Math.abs(proj.distance(vertex) - collision.overlap) < 0.001) {
                let dir = proj.subtract(vertex).normalize()
                if (Math.abs(dir.dot(collision.mtv)) > 0.999) {
                    ctx.moveTo(...proj)
                    ctx.lineTo(...vertex)
                }
            }
        })
    })
    
    rb1faces.vertices.forEach(vertex => {
        rb0faces.edges.forEach(([i0, i1]) => {
            let proj = vertex.project_on_seg(rb0faces.vertices[i0], rb0faces.vertices[i1], true, true)
            if (proj && Math.abs(proj.distance(vertex) - collision.overlap) < 0.001) {
                let dir = proj.subtract(vertex).normalize()
                if (Math.abs(dir.dot(collision.mtv)) > 0.999) {
                  ctx.moveTo(...proj)
                  ctx.lineTo(...vertex)
                }
            }
        })
    })
    ctx.stroke()
//*/
}

class BoxShape {
    constructor(width, height, mass) {
        this.width = width
        this.height = height
        this.mass = mass
        this.moment_of_inertia = this.calculateBoxInertia()

        this.vertices = this.calculateVertices()
        this.normals = this.calculateNormals()
    }

    calculateBoxInertia() {
        // Calculates the inertia of a box shape and stores it in the momentOfInertia variable.
        let m = this.mass
        let w = this.width
        let h = this.height
        return m * (w * w + h * h) / 12
    }

    calculateVertices() {
        const half_size = [this.width, this.height].divide(2)
        return [
            half_size.multiply([ 1, 1]),
            half_size.multiply([-1, 1]),
            half_size.multiply([-1,-1]),
            half_size.multiply([ 1,-1])
        ]
    }

    calculateNormals() {
        let result = new Array(this.vertices.length)
        for (let i = 0; i < this.vertices.length; i++) {
            const v0 = this.vertices[i]
            const v1 = this.vertices[(i + 1) % this.vertices.length]

            const n = v1.subtract(v0).normalize()
            result[i] = [n[1], -n[0]]
        }
        return result
    }

    draw(ctx) {
        ctx.fillRect(
            -this.width  / 2,
            -this.height / 2,
            this.width,
            this.height
        )
    }
}

class AABB {
    constructor() {
        this.min = [0, 0]
        this.max = [0, 0]
    }

    update(vertices) {
        this.min = this.min.map(_ => Number.MAX_SAFE_INTEGER)
        this.max = this.max.map(_ => Number.MIN_SAFE_INTEGER)

        vertices.forEach(vertex => {
            this.min = vertex.min(this.min)
            this.max = vertex.max(this.max)
        })
    }

    draw(ctx) {
        ctx.beginPath()
        ctx.strokeRect(...this.min, ...this.max.subtract(this.min))
        ctx.stroke()
    }
}

class Collider {
    constructor(shape) {
        this.shape = shape

        this.aabb = new AABB()

        // collider verts/normals are kept in world space to make collisions calculations easier.
        this.vertices = []
        this.normals = []

        this.update([0, 0], 0)
    }

    update(position, rotation) {
        // transform shapes vertices/normals to world coords

        const angle_in_radians = rotation * Math.PI / 180
        const c = Math.cos(angle_in_radians)
        const s = Math.sin(angle_in_radians)
        const rot = p => {
            return [
                p[0] * c - p[1] * s,
                p[0] * s + p[1] * c
            ]
        }
        const trans_rot = p => {
            return position.add(rot(p))
        }

        this.vertices = this.shape.vertices.map(trans_rot)
        this.normals = this.shape.normals.map(rot)
        
        this.aabb.update(this.vertices)
    }

    projectToAxis(axis) {
        let min = Number.MAX_SAFE_INTEGER
        let max = Number.MIN_SAFE_INTEGER

        this.vertices.forEach(vertex => {
            const d = vertex.dot(axis)
            min = Math.min(min, d)
            max = Math.max(max, d)
        })

        return { min, max }
    }

    getCulledColliderFaces(cull_dir) {
        let verts = []
        let edges = []
        let norms = []

        let last_i = -1
        for (let i = 0; i < this.vertices.length; i++) {
            if (this.normals[i].dot(cull_dir) > 0) {
                const next_i = (i + 1) % this.vertices.length

                if (i != last_i) {
                    verts.push(i)
                }
                verts.push(next_i)

                edges.push([
                    verts.length - 2,
                    verts.length - 1
                ])

                norms.push(this.normals[i])

                last_i = next_i
            }
        }

        verts = verts.map(v => this.vertices[v])

        return {
            vertices: verts,
            normals: norms,
            edges: edges
        }
    }

    draw(ctx) {
        // draw aabb
        this.aabb.draw(ctx)

        // draw corners
        ctx.beginPath()
        this.vertices.forEach((vertex, i) => {
            ctx.moveTo(...vertex)
            ctx.arc(...vertex, 5, 0, Math.PI * 2)
            ctx.moveTo(...vertex)
            ctx.lineTo(...vertex.add(this.normals[i].multiply(10)))
        })
        ctx.stroke()
    }
}

class RigidBody {
    constructor(position, shape) {
        this.shape = shape
        this.collider = new Collider(shape)

        this.force = [0, 0]
        this.torque = 0

        this.linear_acceleration = [0, 0]
        this.linear_velocity = [0, 0]
        this.position = position
        
        this.angular_acceleration = 0
        this.angular_velocity = 0
        this.angle = 0

        this.forces = []

        this.collider.update(this.position, this.angle)
    }

    addForce(force, position) {
        this.forces.push({ force, position })

        if (position) {
            // r is the 'arm vector' that goes from the center of mass to the point of force application
            let r = position.subtract(this.position)
            let t = r[0] * force[1] - r[1] * force[0]
            
            this.torque += t
        }

        this.force = this.force.add(force)
    }

    update(dt) {
        this.linear_acceleration = this.force.divide(this.shape.mass)
        this.linear_velocity = this.linear_velocity.add(this.linear_acceleration.multiply(dt))
        this.position = this.position.add(this.linear_velocity.multiply(dt))

        this.angular_acceleration = this.torque / this.shape.moment_of_inertia
        this.angular_velocity += this.angular_acceleration * dt
        this.angle += this.angular_velocity * dt

        // add drag
        this.linear_velocity = this.linear_velocity.multiply(0.999)
        this.angular_velocity = this.angular_velocity * 0.999

        this.force = [0, 0]
        this.torque = 0

        this.collider.update(this.position, this.angle)
    }

    draw(ctx) {
        // draw shape
        ctx.save()
        ctx.translate(...this.position)
        ctx.rotate(this.angle * Math.PI / 180)

        ctx.fillStyle = '#fff1'
        this.shape.draw(ctx)

        ctx.restore()
        // draw shape

        // draw collider + aabb
        ctx.strokeStyle = 'blue'
        this.collider.draw(ctx)
        // draw collider + aabb

        // draw debug forces
        this.forces.forEach(({force, position}) => {
            if (position) {
                ctx.strokeStyle = 'green'
                ctx.beginPath()
                ctx.arc(...position, 3, 0, Math.PI * 2)
                ctx.moveTo(...position)
                ctx.lineTo(...position.add(force))
                ctx.stroke()
            } else {
                ctx.strokeStyle = 'yellow'
                ctx.beginPath()
                ctx.arc(...this.position, 3, 0, Math.PI * 2)
                ctx.moveTo(...this.position)
                ctx.lineTo(...this.position.add(force))
                ctx.stroke()
            }
        })
        this.forces.length = 0
        // draw debug forces

        
        ctx.strokeStyle = 'cyan'
        ctx.beginPath()
        ctx.moveTo(...this.position)
        ctx.lineTo(...this.position.add(this.linear_velocity))
        ctx.stroke()
    }
}


let mouse_position = canvas.size().divide(4)
canvas.addEventListener('mousemove', e => {
    mouse_position = [e.clientX, e.clientY]
})

const rigidbody1 = new RigidBody(canvas.size().divide(2), new BoxShape(400, 100, 100))
const rigidbody2 = new RigidBody(mouse_position, new BoxShape(100, 100, 10))

new Simulation((gt, dt) => {
    dt *= 0.01

    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    rigidbody1.addForce([0, 100])
    rigidbody1.addForce([0,-100], [rigidbody1.position[0] + 100, rigidbody1.position[1]])

    const center_dx = canvas.size().divide(2).subtract(rigidbody1.position)
    rigidbody1.addForce(center_dx.multiply(0.3))


    rigidbody2.addForce([0, 10])
    rigidbody2.addForce([0,-10], [rigidbody2.position[0] - 100, rigidbody2.position[1]])
    rigidbody2.addForce(mouse_position.subtract(rigidbody2.position).multiply(0.3))

    rigidbody1.update(dt)
    rigidbody2.update(dt)

    rigidbody1.draw(ctx)
    rigidbody2.draw(ctx)


    //let itr_cnt = 10
    //for (let i = 0; i < itr_cnt; i++) {
        // update rigidbodies + colliders


        //update collision response
        let collision = SAT(rigidbody1, rigidbody2)
        if (collision) {
            //ctx.strokeStyle = 'cyan'
            //ctx.beginPath()
            //ctx.moveTo(...[100, 100])
            //ctx.lineTo(...[100, 100].add(collision.mtv.multiply(collision.overlap)))
            //ctx.stroke()

            ContactPoint(collision)

            // not sure if this is right, but seems to give a reasonable response
            //const max_mass = Math.max(rigidbody1.shape.mass, rigidbody2.shape.mass)
            //const rb1_percent = rigidbody1.shape.mass / max_mass / itr_cnt
            //const rb2_percent = rigidbody2.shape.mass / max_mass / itr_cnt
            //rigidbody1.linear_velocity = rigidbody1.linear_velocity.add(collision.mtv.multiply(-1 * rb2_percent))
            //rigidbody2.linear_velocity = rigidbody2.linear_velocity.add(collision.mtv.multiply( 1 * rb1_percent))
        }
    //}


    // draw cursor
    ctx.strokeStyle = 'white'
    ctx.beginPath()
    ctx.arc(...mouse_position, 5, 0, Math.PI * 2)
    ctx.stroke()
})