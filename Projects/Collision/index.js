let is_mousedown = false
let pos = [0, 0]

canvas.addEventListener('mousedown', e => {
    is_mousedown = true
    pos = [e.clientX, e.clientY]
})

canvas.addEventListener('mousemove', e => {
    if (is_mousedown) {
        pos = [e.clientX, e.clientY]
    }
})

canvas.addEventListener('mouseup', e => {
    is_mousedown = false
})

canvas.addEventListener('mouseleave', e => {
    is_mousedown = false
})

function genRB(i) {
    const vert_count = Math.round(Math.random() * 5 + 3)
    const verts = new Array(vert_count)

    const step = (Math.PI * 2) / vert_count
    for (let i = 0; i < vert_count; i++) {
        const angle = i * step
        verts[i] = [
            Math.cos(angle),
            Math.sin(angle)
        ]
    }

    const is_static = i == 0 ? false : Math.random() > 0.75

    return new RigidPolygon2D(
        [512 * Math.random() + 100, 512 * Math.random() + 100],
        [50, 50],//[50 * Math.random() + 25, 50 * Math.random() + 25],
        verts,
        is_static
    )
}

const rigidbodies = [...Array(10)].map((_,i) => genRB(i))

const sim = new Simulation((gt, dt) => {
    ctx.fillStyle = 'black'
    ctx.fillRect(...canvas.rect())

    if (is_mousedown) {
        rigidbodies[0].set_origin(rigidbodies[0].origin.lerp(pos, 0.01 * dt))
    }

    for (let p = 0; p < 10; p++) { // passes
        for (let i = 0; i < rigidbodies.length; i++) {
            for (let j = i + 1; j < rigidbodies.length; j++) {
                separating_axis_theorem(rigidbodies[i], rigidbodies[j])
            }
        }
    }


    rigidbodies.forEach(rb => rb.render(ctx))

    
    for (let i = 0; i < rigidbodies.length; i++) {
        for (let j = i + 1; j < rigidbodies.length; j++) {
            find_contact_points(ctx, rigidbodies[i], rigidbodies[j])
        }
    }
})