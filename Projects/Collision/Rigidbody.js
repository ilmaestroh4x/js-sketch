class Rigidbody {
    constructor(origin, is_static = false) {
        this.is_static = is_static
        this.set_origin(origin)
    }

    set_origin(origin) {
        if (origin instanceof Array && origin.is_vector()) {
            this.origin = origin.clone()
        }
    }

    render(ctx, color = 'lime') {
        ctx.strokeStyle = this.is_static ? 'red' : color
        ctx.beginPath()
        ctx.arc(this.origin[0], this.origin[1], 3, 0, Math.PI * 2)
        ctx.stroke()
    }
}

class RigidPolygon2D extends Rigidbody {
    constructor(origin, scale, vertices, is_static = false) {
        super(origin, is_static)
        this.set_scale(scale)
        this.set_vertices(vertices)
    }

    set_scale(scale) {
        if (scale instanceof Array && scale.is_vector()) {
            this.scale = scale.map(Math.abs)
            this.calculate_normals()
        }
    }

    set_vertices(vertices) {
        if (vertices instanceof Array && !vertices.some(v => !v.is_vector())) {
            this.vertices = vertices.clone()
            this.calculate_normals()
        }
    }

    calculate_normals() {
        if (this.vertices) {
            this.normals = new Array(this.vertices.length)
            for (let i = 0; i < this.vertices.length; i++) {
                let this_vertex = this.vertices[i]
                    this_vertex = this_vertex.multiply(this.scale)

                let next_vertex = this.vertices[(i + 1) % this.vertices.length]
                    next_vertex = next_vertex.multiply(this.scale)

                let normal = next_vertex.subtract(this_vertex)
                    normal = normal.normalize()
                    normal = [normal[1], -normal[0]]
                
                this.normals[i] = normal
            }
        }
    }

    render(ctx) {
        //render origin
        super.render(ctx, 'magenta')

        //render edges
        ctx.strokeStyle = 'lime'
        ctx.beginPath()
        this.vertices.forEach(vertex => {
            vertex = vertex.multiply(this.scale)
            vertex = vertex.add(this.origin)
            ctx.lineTo(vertex[0], vertex[1])
        })
        ctx.closePath()
        ctx.stroke()

        //render normals
        ctx.strokeStyle = 'cyan'
        ctx.beginPath()

        for (let i = 0; i < this.vertices.length; i++) {
            const this_vertex = this.vertices[i]
            const next_vertex = this.vertices[(i + 1) % this.vertices.length]

            let mid_point = this_vertex.lerp(next_vertex, 0.5)
                mid_point = mid_point.multiply(this.scale)
                mid_point = mid_point.add(this.origin)

            let normal = this.normals[i]
                normal = normal.multiply(10)
                normal = normal.add(mid_point)

            ctx.moveTo(mid_point[0], mid_point[1])
            ctx.lineTo(normal[0], normal[1])
        }

        ctx.stroke()
    }
}

function separating_axis_theorem(rp2d0, rp2d1) {
    if (rp2d0.is_static && rp2d1.is_static) return

    // project all verticies onto axis, getting min and max values
    function projectToAxis(rigidpoly2d, axis) {
        let min = Number.MAX_SAFE_INTEGER
        let max = Number.MIN_SAFE_INTEGER

        let min_vertex = null
        let max_vertex = null

        rigidpoly2d.vertices.forEach(vertex => {
            vertex = vertex.multiply(rigidpoly2d.scale)
            vertex = vertex.add(rigidpoly2d.origin)

            const d = vertex.dot(axis)

            if (min > d) {
                min = d
                min_vertex = vertex
            }

            if (max < d) {
                max = d
                max_vertex = vertex
            }
        })

        return {
            min,
            max,
            min_vertex,
            max_vertex
        }
    }

    let min_overlap = Number.MAX_SAFE_INTEGER
    let collision_normal = null

    rp2d0.normals.some(axis => {
        const mm0 = projectToAxis(rp2d0, axis)
        const mm1 = projectToAxis(rp2d1, axis)

        const overlap = Math.min(mm0.max, mm1.max) - Math.max(mm0.min, mm1.min)
        if (min_overlap > overlap) {
            min_overlap = overlap
            collision_normal = axis
        }

        return !(mm0.min < mm1.max && mm0.max > mm1.min)
    })

    rp2d1.normals.some(axis => {
        const mm0 = projectToAxis(rp2d0, axis)
        const mm1 = projectToAxis(rp2d1, axis)

        const overlap = Math.min(mm0.max, mm1.max) - Math.max(mm0.min, mm1.min)
        if (min_overlap > overlap) {
            min_overlap = overlap
            collision_normal = axis
        }

        return !(mm0.min < mm1.max && mm0.max > mm1.min)
    })

    collision_normal = collision_normal.normalize()

    if (min_overlap > 0) {
        const overlap_dir = rp2d0.origin.subtract(rp2d1.origin).normalize()
        const overlap_sign = Math.sign(overlap_dir.dot(collision_normal))

        const signed_overlap = min_overlap * overlap_sign

        let rp2d0_offset = null
        let rp2d1_offset = null

        if (rp2d0.is_static) {
            rp2d1_offset = collision_normal.multiply(-signed_overlap)
        }

        if (rp2d1.is_static) {
            rp2d0_offset = collision_normal.multiply( signed_overlap)
        }
        
        if (!rp2d0.is_static && !rp2d1.is_static) {
            const signed_half_overlap = signed_overlap / 2
            rp2d0_offset = collision_normal.multiply( signed_half_overlap)
            rp2d1_offset = collision_normal.multiply(-signed_half_overlap)
        }

        rp2d0.set_origin(rp2d0.origin.add(rp2d0_offset))
        rp2d1.set_origin(rp2d1.origin.add(rp2d1_offset))
    }
}

function find_contact_points(ctx, rp2d0, rp2d1) {
    const axis = rp2d0.origin.subtract(rp2d1.origin).normalize()
    
    function cull_rigidbody2d(rigidpoly2d, normal) {
        const { vertices, normals, scale, origin } = rigidpoly2d

        let verts = []
        let edges = []
        let norms = []

        let last_i = -1
        for (let i = 0; i < vertices.length; i++) {
            if (normals[i].dot(normal) > 0) {
                const next_i = (i + 1) % vertices.length

                if (i != last_i) {
                    verts.push(i)
                }
                verts.push(next_i)

                edges.push([
                    verts.length - 2,
                    verts.length - 1
                ])

                norms.push(normals[i])

                last_i = next_i
            }
        }

        // convert array of vertex indices to world positions
        verts = verts.map(v => vertices[v].multiply(scale).add(origin))

        return { verts, edges, norms }
    }

    const ven0 = cull_rigidbody2d(rp2d0, axis.multiply(-1))
    const ven1 = cull_rigidbody2d(rp2d1, axis.multiply( 1))

    const lw = ctx.lineWidth
    ctx.lineWidth = 1
    ctx.strokeStyle = 'red'
    ctx.beginPath()

    ven0.verts.forEach(vert => {
        ven1.edges.forEach(([s_i, e_i], i) => {
            const v1 = ven1.verts[s_i]
            const v2 = ven1.verts[e_i]

            const seg_len = v1.distance(v2)
            const v1_len = vert.distance(v1)
            const v2_len = vert.distance(v2)

            if (Math.abs((v1_len + v2_len) - seg_len) < 0.001) {
                ctx.moveTo(...vert)
                ctx.lineTo(...(vert.add(ven1.norms[i].multiply(-10))))
                ctx.moveTo(...vert)
                ctx.arc(...vert, 3, 0, Math.PI * 2)
            }
        })
    })

    ven1.verts.forEach(vert => {
        ven0.edges.forEach(([s_i, e_i], i) => {
            const v1 = ven0.verts[s_i]
            const v2 = ven0.verts[e_i]

            const seg_len = v1.distance(v2)
            const v1_len = vert.distance(v1)
            const v2_len = vert.distance(v2)

            if (Math.abs((v1_len + v2_len) - seg_len) < 0.001) {
                ctx.moveTo(...vert)
                ctx.lineTo(...(vert.add(ven0.norms[i].multiply(-10))))
                ctx.moveTo(...vert)
                ctx.arc(...vert, 3, 0, Math.PI * 2)
            }
        })
    })

    ctx.stroke()
    ctx.lineWidth = lw
}