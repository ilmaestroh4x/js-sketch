const bounding_functions = {
    contains: {
        AABB: {
            Array(aabb, vec) {
                const min = aabb.min()
                const max = aabb.max()
                return !vec.some((p, i) => !p.is_between(min[i], max[i]))
            },
            AABB(aabb0, aabb1) {
                const min_aabb0 = aabb0.min()
                const max_aabb0 = aabb0.max()
                const min_aabb1 = aabb1.min()
                const max_aabb1 = aabb1.max()
                for (let i = 0; i < aabb0.dimensions(); i++) {
                    if (min_aabb0[i] > min_aabb1[i] ||
                        max_aabb0[i] < max_aabb1[i]) return false
                }
                return true
            },
            BoundingSphere(aabb, boundingSphere) {
                const tmp_scale = aabb.scale.subtract(boundingSphere.radius * 2)
                if (tmp_scale.some(value => value < 0)) return false
                const tmp_aabb = new AABB(aabb.origin, tmp_scale)
                return bounding_functions.contains.AABB.Array(tmp_aabb, boundingSphere.origin)
            }
        },
        BoundingSphere: {
            Array(boundingSphere, vec) {
                return boundingSphere.origin.distance(vec) < boundingSphere.radius
            },
            AABB(boundingSphere, aabb) {
                const l = aabb.dimensions()
                const o = i => i.to_binary_array(l).map(v => v - 0.5)
                return ![...Array(Math.pow(2, l))].map((_, i) => {
                    return aabb.origin.add(aabb.scale.multiply(o(i)))
                }).some(p => {
                    return !bounding_functions.contains.BoundingSphere.Array(boundingSphere, p)
                })
            },
            BoundingSphere(boundingSphere0, boundingSphere1) {
                return boundingSphere0.origin.distance(boundingSphere1.origin) < boundingSphere0.radius - boundingSphere1.radius
            }
        }
    },
    intersects: {
        AABB: {
            Array(aabb, vec) {
                return bounding_functions.contains.AABB.Array(aabb, vec)
            },
            AABB(aabb0, aabb1) {
                const min_aabb0 = aabb0.min()
                const max_aabb0 = aabb0.max()
                const min_aabb1 = aabb1.min()
                const max_aabb1 = aabb1.max()
                for (let i = 0; i < aabb0.dimensions(); i++) {
                    if (min_aabb0[i] > max_aabb1[i] ||
                        max_aabb0[i] < min_aabb1[i]) return false
                }
                return true
            },
            BoundingSphere(aabb, boundingSphere) {
                const min = aabb.min()
                const max = aabb.max()
                const p = [...Array(aabb.dimensions())].map((_, i) => {
                    return boundingSphere.origin[i].clamp(min[i], max[i])
                })
                return bounding_functions.contains.BoundingSphere.Array(boundingSphere, p)
            },
        },
        BoundingSphere: {
            Array(boundingSphere, vec) {
                return bounding_functions.contains.BoundingSphere.Array(boundingSphere, vec)
            },
            AABB(boundingSphere, aabb) {
                return bounding_functions.intersects.AABB.BoundingSphere(aabb, boundingSphere)
            },
            BoundingSphere(boundingSphere0, boundingSphere1) {
                return boundingSphere0.origin.distance(boundingSphere1.origin) < boundingSphere0.radius + boundingSphere1.radius
            }
        }
    }
}

class Bounds {
    constructor(origin) {
        this.set_origin(origin)
    }

    set_origin(origin) {
        if (origin instanceof Array && origin.is_vector()) {
            this.origin = origin
        }
    }

    dimensions() {
        return this.origin.length
    }

    contains(other) {
        if (other instanceof Bounds || other instanceof Array) {
            const t_type = this.constructor.name
            const o_type = other.constructor.name
            return bounding_functions.contains[t_type][o_type](this, other)
        } else {
            return false
        }
    }

    intersects(other) {
        if (other instanceof Bounds || other instanceof Array) {
            const t_type = this.constructor.name
            const o_type = other.constructor.name
            return bounding_functions.intersects[t_type][o_type](this, other)
        } else {
            return false
        }
    }
}

class AABB extends Bounds {
    constructor(origin, scale) {
        super(origin)
        this.set_scale(scale)
    }

    set_scale(scale) {
        if (scale instanceof Array && scale.is_vector()) {
            this.scale = scale.map(Math.abs)
        }
    }

    half_scale() {
        return this.scale.divide(2)
    }

    min() {
        return this.origin.subtract(this.half_scale())
    }

    max() {
        return this.origin.add(this.half_scale())
    }

    render(ctx, color = 'blue') {
        ctx.strokeStyle = color
        const p = this.min().map(v => Math.floor(v) + 0.5)
        const s = this.scale.map(Math.floor)
        ctx.strokeRect(p[0], p[1], s[0], s[1])
    }
}

class BoundingSphere extends Bounds {
    constructor(origin, radius) {
        super(origin)
        this.set_radius(radius)
    }

    set_radius(radius) {
        if (isFinite(radius)) {
            this.radius = radius
        }
    }

    render(ctx, color = 'blue') {
        ctx.strokeStyle = color
        ctx.beginPath()
        ctx.arc(...this.origin, this.radius, 0, Math.PI * 2)
        ctx.stroke()
    }
}