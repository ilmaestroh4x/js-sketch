class NTree {
    constructor(boundary_aabb, capacity = 10, depth = 0) {
        this.boundary = boundary_aabb
        this.capacity = capacity
        this.depth = depth
        this.elements = []
        this.leaves = []
    }

    subdivide() {
        if (this.leaves.length > 0) return // Already subdivided.

        const dims = this.boundary.dimensions()
        const sub_div_count = Math.pow(2, dims)

        const half_scale = this.boundary.half_scale()

        for (let i = 0; i < sub_div_count; i++) {
            const offset = half_scale.multiply(i.to_binary_array(dims).map(v => v - 0.5))
            const position = this.boundary.origin.add(offset)
            const boundary = new AABB(position, half_scale)
            this.leaves.push(new NTree(boundary, this.capacity, this.depth + 1))
        }
    }

    insert(element) {
        let result = false

        if (this.boundary.contains(element.boundary)) {
            if (this.elements.length < this.capacity ) {
                this.elements.push(element)
                result = true
            } else {
                this.subdivide()
                result = this.leaves.some(l => l.insert(element))

                if (!result) { // couldn't insert into any leaves
                    // attempt to move some of the other elements down into the leaves
                    let success_list = []
                    this.elements.forEach((e, i) => {
                        if (this.leaves.some(l => l.insert(e))) {
                            success_list.unshift(i)
                        }
                    })
                    //if (success_list.length > 0) {
                    //    console.log(`Successfully moved ${success_list.length} element(s) to leaves.`)
                    //}
                    success_list.forEach(i => this.elements.splice(i, 1))

                    // retry inserting element
                    if (this.elements.length < this.capacity ) {
                        this.elements.push(element)
                        result = true
                    }
                }
            }
        }

        return result
    }

    insert_point(x, y, data) {
        return this.insert({ boundary: [x, y], data })
    }

    query(query_boundary, results = []) {
        if (query_boundary.intersects(this.boundary)) {
            this.elements.forEach(e => {
                if (query_boundary.intersects(e.boundary)) {
                    results.push(e)
                }
            })

            this.leaves.forEach(l => l.query(query_boundary, results))
        }

        return results
    }

    forEachNode(on_node_callback) {
        on_node_callback(this)
        this.leaves.forEach(leaf => leaf.forEachNode(on_node_callback))
    }

    render(ctx, color) {
        this.boundary.render(ctx, color)
        //this.elements.forEach(e => e.boundary.render(ctx, 'red'))
        this.leaves.forEach(leaf => leaf.render(ctx, color))
    }
}