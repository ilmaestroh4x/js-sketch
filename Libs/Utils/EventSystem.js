class EventSystem {
    constructor() {
        this.handlers = {}
    }

    is_str(arg) {
        return typeof arg === 'string'
    }

    is_fn(arg) {
        return typeof arg === 'function'
    }

    has(eventName) {
        return is_str(eventName) && !!this.handlers[eventName]
    }

    emit(eventName, data) {
        if (is_str(eventName)) {
            const handlers = this.handlers[eventName]
            if (handlers instanceof Array) {
                handlers.forEach(handler => handler(data))
            }
        }
    }

    on(eventName, callback) {
        if (is_str(eventName) && is_fn(callback)) {
            if (this.handlers[eventName] === undefined) {
                this.handlers[eventName] = []
            }
            this.handlers[eventName].push(callback)
        }
    }

    clear(eventName) {
        if (is_str(eventName)) {
            delete this.handlers[eventName]
        }
    }
}