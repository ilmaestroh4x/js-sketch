class Simulation {
    constructor(callback, autostart = true) {
        this.last_time  = 0
        this.running    = false
        this.callback   = callback

        if (autostart) {
            this.start()
        }
    }

    step(global_time) {
        this.callback(global_time, global_time - this.last_time)
        this.last_time = global_time
        if (this.running) {
            window.requestAnimationFrame(t => this.step(t))
        }
    }

    start() {
        if (!this.running) {
            this.running = true
            this.step(this.last_time)
        }
    }

    stop() {
        this.running = false
    }
}