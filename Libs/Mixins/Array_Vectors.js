Array.prototype.is_vector = function() {
    return this.length > 0 && !this.some(isNaN)
}

Array.prototype.clone = function() {
    return this.slice(0)
}

Array.prototype.abs = function() {
    return this.map(Math.abs)
}

Array.prototype.equals = function(other) {
    return !this.some((value, index) => value !== other[index])
}

/*
    param: 'min' can be a vector of the same length or scalar value
    param: 'max' can be a vector of the same length or scalar value
    returns: new array containing result
*/
Array.prototype.clamp = function(min, max) {
    return this.map((value, index) => {
        const t_min = (min instanceof Array) ? min[index] : min
        const t_max = (max instanceof Array) ? max[index] : max
        return Math.max(t_min, Math.min(value, t_max))
    })
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.min = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => {
            value = Math.min(value, (other instanceof Array) ? other[index] : other)
        })
        return value
    })
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.max = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => {
            value = Math.max(value, (other instanceof Array) ? other[index] : other)
        })
        return value
    })
}

/*
    param: 'other' can be a vector of the same length or scalar value
    param: 'comparator' is a 'function(lhs, rhs)' that compares the right hand side(rhs) to the left hand side(lhs)
    returns: new array containing result
*/
Array.prototype.comparator = function(other, comparator) {
    return this.map((value, index) => {
        return comparator(value, (other instanceof Array) ? other[index] : other)
    })
}

Array.prototype.all = function(required_value) {
    return this.every(value => value == required_value)
}

Array.prototype.true = function() {
    return this.all(true)
}

Array.prototype.false = function() {
    return this.all(false)
}

Array.prototype.random_normal = function() {
    return this.map(_ => Math.random() * 2 - 1).normalize()
}

Array.prototype.sqr_magnitude = function() {
    return this.reduce((acc, cur) => acc + cur * cur, 0)
}

Array.prototype.magnitude = function() {
    return Math.hypot(...this)
    //return Math.sqrt(this.sqr_magnitude())
}

Array.prototype.sqr_distance = function(other) {
    return this.subtract(other).sqr_magnitude()
}

Array.prototype.distance = function(other) {
    return this.subtract(other).magnitude()
}

Array.prototype.normalize = function() {
    return this.divide(Math.max(Number.EPSILON, this.magnitude()))
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.add = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => value += (other instanceof Array) ? other[index] : other)
        return value
    })
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.subtract = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => value -= (other instanceof Array) ? other[index] : other)
        return value
    })
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.multiply = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => value *= (other instanceof Array) ? other[index] : other)
        return value
    })
}

/*
    param: 'others' can be multiple vectors of the same length and/or scalar values
    returns: new array containing result
*/
Array.prototype.divide = function(...others) {
    return this.map((value, index) => {
        others.forEach(other => value /= (other instanceof Array) ? other[index] : other)
        return value
    })
}

Array.prototype.dot = function(other) {
    return this.reduce((acc, cur, index) => acc + cur * other[index], 0)
}

Array.prototype.lerp = function(other, t) {
    return this.map((value, index) => value + t * (other[index] - value))
}

Array.prototype.project = function(other) {
    return other.multiply(this.dot(other) / other.sqr_magnitude())
}

Array.prototype.project_on_seg = function(start, end, is_clamped = true, is_discard = false) {
    const point     = this.subtract(start)
    const segment   =  end.subtract(start)
    
    let projection  = point.project(segment).add(start)

    if (is_clamped || is_discard) {
        const dist_seg = segment.magnitude()
        const dist_p2s = projection.distance(start)
        const dist_p2e = projection.distance(end)

        if (dist_p2s > dist_seg ||
            dist_p2e > dist_seg) {
            projection = dist_p2s < dist_p2e ? start : end

            if (is_discard) {
                return null
            }
        }
    }

    return projection
}

Array.prototype.reflect = function(surface, normalize = true) {
    let v = this
    let n = surface

    if (normalize) {
        v = v.normalize()
        n = n.normalize()
    }
    
    return v.subtract(n.multiply(2, v.dot(n)))
}

Array.prototype.bresenham_line = function(other, allow_diagonal_step = true) {
    let result = []

    let x0 = this[0]
    let y0 = this[1]

    let x1 = other[0]
    let y1 = other[1]

    let x_dist =  Math.abs(x1 - x0)
    let y_dist = -Math.abs(y1 - y0)

    let x_step = x0 < x1 ? 1 : -1
    let y_step = y0 < y1 ? 1 : -1

    let error = x_dist + y_dist
    let error_2

    result.push([x0, y0])

    if (allow_diagonal_step) {
        while (x0 != x1 || y0 != y1) {
            error_2 = error * 2

            if (error_2 > y_dist) {
                error += y_dist
                x0 += x_step
            }

            if (error_2 < x_dist) {
                error += x_dist
                y0 += y_step
            }

            result.push([x0, y0])
        }
    } else {
        while (x0 != x1 || y0 != y1) {
            error_2 = error * 2

            if (error_2 - y_dist > x_dist - error_2) {
                error += y_dist
                x0 += x_step
            } else {
                error += x_dist
                y0 += y_step
            }

            result.push([x0, y0])
        }
    }

    return result
}