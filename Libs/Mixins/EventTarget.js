// multi event listener
EventTarget.prototype.addEventListeners = function(types, listener, ...extras) {
    // use set to filter duplicates
    [...new Set(types)].forEach(type => this.addEventListener(type, listener, ...extras))
}