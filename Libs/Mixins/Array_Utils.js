Array.prototype.empty = function() {
    return this.length == 0
}

Array.prototype.peek = function() {
    return this[this.length - 1]
}

Array.prototype.iterator = function(callback) {
    if (this.some(v => !Number.isInteger(v) || v < 0)) {
        throw Error(`Error: 'Array.prototype.iterator' is only usable on an array of positive integers.`)
    }

    if (typeof callback !== 'function') {
        throw Error(`Error: 'Array.prototype.iterator' 'callback' parameter must be a function.`)
    }
    
    const indices = this.map(v => 0)
    const max_i = this.reduce((a, c) => a * c)

    for (let i = 0; i < max_i; i++) {
        callback(indices, i)

        let rollover = false
        let rollover_i = this.length - 1
        do {
            indices[rollover_i]++
            if (indices[rollover_i] == this[rollover_i]) {
                indices[rollover_i] = 0
                rollover = true
                rollover_i--
            } else {
                rollover = false
            }
        } while (rollover && rollover_i >= 0)
    }
}

Array.prototype.set_at = function(indices, value) {
    if (indices instanceof Array && indices.some(v => !Number.isInteger(v) || v < 0)) {
        throw Error(`Error: 'Array.set_at', '...indices' must be positive integer(s).`)
    }

    let temp = this
    indices.forEach((i, j) => {
        if (j < indices.length - 1) {
            temp = temp[i]
        } else {
            temp[i] = value
        }
    })
}

Array.prototype.get_at = function(indices) {
    if (indices instanceof Array && indices.some(v => !Number.isInteger(v) || v < 0)) {
        throw Error(`Error: 'Array.get_at', '...indices' must be positive integer(s).`)
    }

    let temp = this
    indices.forEach(i => {
        temp = temp[i]
    })
    return temp
}


Array.create_matrix = function(...sizes) {
    if (sizes.length > 0) {
        return [...Array(sizes.shift())].map(() => Array.create_matrix(...sizes))
    } else {
        return 0
    }
}

Array.prototype.deep_forEach = function(callback, indices = []) {
    this.forEach((e, i) => {
        if (e instanceof Array) {
            e.deep_forEach(callback, [...indices, i])
        } else {
            callback(e, [...indices, i])
        }
    })
}

Array.prototype.deep_map = function(callback, indices = []) {
    return this.map((e, i) => {
        if (e instanceof Array) {
            return e.deep_map(callback, [...indices, i])
        } else {
            return callback(e, [...indices, i])
        }
    })
}

Array.prototype.deep_copy = function() {
    return this.deep_map(value => value)
}