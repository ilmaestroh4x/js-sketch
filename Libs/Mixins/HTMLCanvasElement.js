HTMLCanvasElement.prototype.size = function() {
    return [ this.width, this.height ]
}

HTMLCanvasElement.prototype.rect = function() {
    return [ 0, 0, ...this.size() ]
}

HTMLCanvasElement.prototype.offscreen_render = function(width, height, callback) {
    const ctx = this.getContext('2d')
    this.width  = width
    this.height = height

    callback(this, ctx)

    return this
}