ImageData.prototype.getPixel = function(x, y, includeAlpha = false) {
    const i = (y * this.width + x) * 4
    return this.data.slice(i, i + (includeAlpha ? 4 : 3))
}

ImageData.prototype.setPixel = function(x, y, color) {
    const i = (y * this.width + x) * 4
    for (let j = 0; j < Math.min(color.length, 4); j++)
        this.data[i + j] = color[j]
}