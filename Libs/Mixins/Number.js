Number.prototype.is_between = function(...values) {
    const value = Number(this)
    return Math.min(...values) <= value && value <= Math.max(...values)
}

Number.prototype.clamp = function(min, max) {
    const value = Number(this)
    return Math.max(min, Math.min(value, max))
}

Number.prototype.to_binary_array_old = function(digits) {
    const value = Number(this)
    const MAX_PAD = Number.MAX_SAFE_INTEGER.toString(2).length
    const bit_str = value.toString(2).padStart(MAX_PAD, '0')
    return bit_str.substr(bit_str.length - digits).split('').map(Number)
}

// Number must be whole number, decimal places not supported.
Number.prototype.to_binary_array = function(digits) {
    const value = Number(this)
    const result = new Array(digits)
    for (let i = 0; i < digits; i++) {
        result[digits - 1 - i] = (value >> i) & 1 ? 1 : 0
    }
    return result
}

Number.prototype.map_to_range = function(from_min, from_max, to_min, to_max) {
    const value = Number(this)
    return (value - from_min) / (from_max - from_min) * (to_max - to_min) + to_min
}